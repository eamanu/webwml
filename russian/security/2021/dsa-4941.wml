#use wml::debian::translation-check translation="158226aad5fbf7d8645b13853a75c3562078bbc7" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В ядре Linux было обнаружено несколько уязвимостей, которые
могут приводить к повышению привилегий, отказу в обслуживании или утечкам
информации.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36311">CVE-2020-36311</a>

    <p>Была обнаружена уязвимость в подсистеме KVM для ЦП AMD, позволяющая
    злоумышленнику вызывать отказ в обслуживании при включении удаления
    большой виртуальной машины SEV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

    <p>Норберт Слусарек сообщил о состоянии гонки в сетевом протоколе
    CAN BCM, позволяющем локальному злоумышленнику повышать
    привилегии.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

    <p>Сотрудники Qualys Research Labs обнаружили уязвимость преобразования
    size_t-to-int в прослойке файловой системы ядра Linux.
    Непривилегированный локальный злоумышленник, способный создавать, монтировать и удалять
    сложно вложенную структуру каталогов, чей полный путь превосходит 1ГБ, может
    использовать эту уязвимость для повышения привилегий.</p>

    <p>Подробности можно найти в рекомендации Qualys по адресу
    <a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">\
    https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

    <p>Норберт Слусарек обнаружил утечку информации в сетевом протоколе
    CAN BCM. Локальный злоумышленник может использовать эту уязвимость
    для получения чувствительной информации из стековой памяти ядра.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 4.19.194-3.</p>

<p>Рекомендуется обновить пакеты linux.</p>

<p>С подробным статусом поддержки безопасности linux можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4941.data"

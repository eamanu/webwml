# From: John S4 Hosting <john@s-4.host>
# Submission email: https://lists.debian.org/debian-www/2021/07/msg00031.html

<define-tag pagetitle>S4 Hosting, Lithuania</define-tag>
<define-tag webpage>https://s-4.host</define-tag>

#use wml::debian::users

<p>
We are an ethical and sustainable hosting company and until recently we've run
most of our web servers on CentOS, together with a mixture of other distros for
things like mail servers and backup servers.
</p>

<p>
Over the last year as our company has grown a bit and with the changes to the
release models for CentOS we have been actively looking for a new solution and
planned to bring all of our servers onto one common Linux distro. We have done
a lot of research and built a lot of test servers and ended up at the decision
that Debian is definitely the best choice for us, and we are migrating
everything over to Debian based servers. 
</p>

<p>
Open source software has been at the centre of our ethos since the beginning
but is something that we are becoming more and more focused on over time. Where
that can be open source projects that are run by foundations or other
communities without being dependent on or owned by a commercial company then so
much the better, and nothing can compare to Debian in this regard. 
</p>

<p>
Since we are hosting client sites and production mail servers, security and
stability are also hugely important to us and again we haven't found anything
else that can match Debian when it comes to stability.
</p>

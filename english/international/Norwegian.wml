#use wml::debian::template title="Debian's Norwegian Corner"
# Updated by Hans F. Nordhaug <hansfn@gmail.com>, 2008-2013.

    <p>On this page you will find information for Norwegian Debian
    users.  If you have something you think belongs here, please write
    one of the Norwegian <a href="#translators">translators</a>.</p>

    <h2>Mailing lists</h2>

    <p>Debian does not currently have any official mailing lists in
    Norwegian.  If there is any interest we can create one or more,
    for users and/or developers.</p>

    <h2>Links</h2>

    <p>Some links which may be of interest for Norwegian Debian users:</p>

    <ul>

    <!-- 
      The organization Linux Norge does not seem to exist any longer.
      The domain linux.no does work, but it is completely outdated.
    -->
    <!-- li><a href="http://www.linux.no/">Linux Norway</a><br>
	<em>"A volunteer organization which aims at spreading
	information on the Linux operating system in Norway."</em>
	Hosts, among other things, the 
    </li -->

    <li><a href="https://nuug.no/">Norwegian UNIX User Group</a>. Visit their Wiki page about
        <a href="https://wiki.nuug.no/likesinnede-oversikt">similar organizations</a>
        that lists most/all active Linux user groups in Norway.
    </li>

    </ul>

    <h2>Norwegian contributors to the Debian Project</h2>
    
    <h3>Currently active Norwegian Debian Developers:</h3>

    <ul>
      <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
      <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
      <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
      <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
      <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
      <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
      <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
      <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
      <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
      <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
    </ul>

    <p>This list is updated infrequently so you might want to check the
    <a href="https://db.debian.org/">Debian Developers Database</a> - just
    select Norway as country.</p>

    <h3>Former Norwegian Debian Developers:</h3>

    <ul>
      <li>Morten Hustvei</li>
      <li>Ole J. Tetlie</li>
      <li>Peter Krefting</li>
      <li>Tom Cato Amundsen</li>
      <li>Tore Anderson</li>
      <li>Tor Slettnes</li>
     </ul>

    <a id="translators"></a>
    <h3>Translators:</h3>

    <p>The Debian web pages are currently translated to Norwegian by:</p>

    <ul>
      <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
    </ul>

    <p>
      <!-- Debian currently does not have anyone that translates its web pages
      into Norwegian. -->
      If you are interested in translating, please begin by reading the
      <a href="$(DEVEL)/website/translating">information for translators</a>.
    </p>

    <p>
      The Debian web pages was earlier translated by:
    </p>

    <ul>
      <li>Cato Auestad</li>
      <li>Stig Sandbeck Mathisen</li>
      <li>Tollef Fog Heen</li>
      <li>Tor Slettnes</li>
    </ul>

    <p>If you would like to help out, or if you know of someone else
    who in one way or another is involved in the Debian project,
    please contact one of us.</p>


<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was found that freeimage, a graphics library, was affected by the
following two security issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Heap buffer overflow caused by invalid memcpy in PluginTIFF. This
    flaw might be leveraged by remote attackers to trigger denial of
    service or any other unspecified impact via crafted TIFF data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Stack exhaustion caused by unwanted recursion in PluginTIFF. This
    flaw might be leveraged by remote attackers to trigger denial of
    service via crafted TIFF data.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 3.17.0+ds1-5+deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.18.0+ds2-1+deb10u1.</p>

<p>We recommend that you upgrade your freeimage packages.</p>

<p>For the detailed security status of freeimage please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"
# $Id: $

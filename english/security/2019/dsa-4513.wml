<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Stefan Metzmacher discovered a flaw in Samba, a SMB/CIFS file, print,
and login server for Unix. Specific combinations of parameters and
permissions can allow user to escape from the share path definition and
see the complete '/' filesystem. Unix permission checks in the kernel
are still enforced.</p>

<p>Details can be found in the upstream advisory at
<a href="https://www.samba.org/samba/security/CVE-2019-10197.html">\
https://www.samba.org/samba/security/CVE-2019-10197.html</a></p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2:4.9.5+dfsg-5+deb10u1.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4513.data"
# $Id: $

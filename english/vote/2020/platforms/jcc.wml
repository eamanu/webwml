#use wml::debian::template title="Platform for Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>DPL Platform</SMALL><BR>
    <SMALL>2020-03-15</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<DIV CLASS="summary">
<B>Broad goals</B>:
<UL CLASS="itemize">
<LI CLASS="li-itemize"> <b>Continue to do what Debian does well.</b> Technical excellence. Promote free software. Packaging. Make new releases. Stable release updates.</LI>
<LI CLASS="li-itemize"> <b>Make Debian attractive to contributors.</b> Make it an interesting, rewarding and welcoming project to participate in for both existing contributors and new ones alike. Bring better visibility to good work that's being done.</LI>
<LI CLASS="li-itemize"> <b>Reduce bottlenecks that affect our contributors.</b> Gather frequent feedback on what's holding individuals back from doing what they care about in Debian, and find ways to reduce that friction. Improve on-line collaboration.</LI>
<LI CLASS="li-itemize"> <b>Improve project housekeeping.</b> Have better visibility and transparency when it comes to our spending and available funds. Keep better track of our local teams. Improve DPL updates.</LI>
</UL>
</DIV>


<H2 CLASS="section">1. Personal Introduction</H2>

<P>Hi, my name is Jonathan Carter, also known as <EM>highvoltage</EM>, with the Debian account name of <EM>jcc</EM>, and I'm running for DPL. </P>

<P>I am a 38 year old hacker and a Debian Developer who have various areas of interest within the Debian project.</P>


<H2 CLASS="section">2. Why I'm running for DPL</H2>

<P>I explained my motivation for running for DPL in my
<a href="https://lists.debian.org/debian-vote/2020/03/msg00007.html">self-nomination mail</a>.
In short, I think that this year, we should focus on stability within our community, and aspire to an environment
where contributors and their ideas are allowed to flourish.
</P>


<H2 CLASS="section">3. Approach</H2>

<P>These ideas are starting points. They can evolve (or even be scrapped entirely) based on further discussion
within the project.
</P>


<H3 CLASS="subsection"> 3.1. Community improvements. </H3>


<P><B>3.1.1. Initiate a public discussion on our membership nomenclature.</B></P>

<P>The term "Debian Developer" has become dated and doesn't reflect the way that the rest of the
world uses the term "developer" anymore. For example, an "Android developer" or "iOS developer"
isn't someone who works for those projects upstream, but someone who develops for
those platforms.
</P>

<P>The term also leads to oddities like calling our project members who don't have upload rights
"Non-uploading Debian Developers", and calling package maintainers with partial upload rights who
aren't members "Debian Maintainers". It's also possible for someone to be a "Debian Maintainer and
Non-uploading DD", which is a really confusing way to convey their status in the project, especially
to outsiders.
</P>

<P>I appreciate that the term DD comes with a lot of clout, and I think we can still preserve that with
better terminology. Suggestions like DPM for Debian Project Member have floated around in previous discussions.
I think it's time that we have a proper discussion about this as a project, find a set of words
and acronyms that work well, and work out the impact this will have on our project documentation.
</P>

<P>Depending on outcomes of the discussion and how our members feel about it, I'm willing to drive it
towards a general resolution.
</P>


<P><B>3.1.2. Promote mentorship. </B></P>

<p>I mentioned before that the status of being a Debian Developer comes with a lot of clout. I'd
like to foster a culture in Debian where reviewing and mentoring others becomes as highly regarded,
and that more Debianites would be motivated to spend more time on this.
</P>

<P>Thousands of Debian contributors aren't Debian Developers, and often wait quite some time for
their packages to be reviewed and sponsored. Improving the turnaround time on this will help
more contributors to eventually become Debian Developers, who can in turn help reduce the reviewing
burden.
</P>

<P>The BTS also has a huge backlog of patches that sadly often get subjected to bitrot by the
time someone gets to look at them. This is something that we need to change.
</P>

<P>We should encourage each other to apply patches, sponsor uploads and organise bug squashing
parties (BSPs). BSPs and related events are really important to the success of Debian
and I intend to promote that.
</P>


<P><B>3.1.3. Improve online participation.</B></P>

<P>In person events (such as BSPs as an example), work great in person because we get to get away from our
usual distractions in life and have high-bandwidth access to other Debian people who may be around.
However, there's no reason why we couldn't also have similar events online. If it's structured
correctly it could be both productive and fun. There are better solutions that exist now that make it easier to share
quick screenshots or video clips, and that we should consider these new solutions in addition to current tools like IRC.
Sure, you could post links to those in IRC, but you have to view them in external programs and you get no
previews, which make it more cumbersome.
</P>

<P>Due to logistics, ethical considerations, finances or responsibilities, travel is simply never
a good option for many of our contributors even under normal circumstances. At the time of writing, we're
also facing quarantine and travel restrictions in regions and organisations
around the world due to the SARS-CoV-2 virus that results in the COVID-19 disease. At least in the
short-term, this will no doubt have an effect on our ability to gather in groups to do our work
or simply to travel to another country. This crisis makes this year an especially important year to
consider additional collaboration tooling.
</P>

<P>Great online tools aren't a match for getting together in person, but if implemented well it could
be an acceptable 2nd prize when meeting in person isn't an option. Perhaps an additional annual online
DebConf, off-set by 6 months to the main DebConf, could be considered.
This will help make Debian a bit more inclusive for those who can't travel.
</P>


<P><B>3.1.3. Local teams. </B></P>

<P>There's been a few attempts over the years to start a centralised team that takes care of local teams globally.
Such a team would maintain a census of local teams and keeping their records up to date. They would also help
bootstrapping new local teams so that they start on a good footing. They could also print Debian stickers, flyers
and posters that could be distributed to the local teams.
</P>

<P>I intend to reach out to people who have previously done work on this, and help to reboot a local
groups team, encouraging them to get together for a sprint and/or organise some sessions at DebConf.</P>

<P>We have some very strong local groups already, and those won't be interfered with,
but I believe that there could be more strong local groups with a bit of support and encouragement.
</P>


<P><B>3.1.4. Better onboarding. </B></P>

<P>I mention onboarding of local groups in the point above, but we could also have better onboarding for
individuals. Debian is still too confusing and often seem intimidating to new contributors, and it doesn't have to be.
</P>

<P>It's still far too difficult for a newcommer to learn how the Debian project works, how to find
something interesting to work on and then find the right people to work with.
</P>

<P>I don't have any proposed solutions for this, but I think that we should keep having discussions about
this at general events like DebConf and throughout the year. Something as basic as leaflets and posters that
explain core Debian concepts could help a lot of people.
</P>


<P><B>3.1.5. Innovation and experiments.</B></P>

<P>I think that Debian could evolve into a project that has a reputation for allowing experimentation and
innovation to flourish.
</P>

<P>If a project member needs financial assistance in order to turn an innovation into reality, I intend
to be reasonably liberal in approving such expenditures in line with our policies.
</P>

<P>I believe it should be easier for a Debian Developer to get a VPS instance for experimentation
without having to pay for it. There are many small barriers like these that we could reduce.
</P>


<H3 CLASS="subsection"> 3.2. Improve reporting. </H3>


<P><B>3.2.1. Better financial understanding.</B>

<P>In the past, there have been requests for better understanding of how Debian spends funds,
and how much money is available at a given time.
</P>

<P>This is a problem that some of the previous DPLs have also seeked to solve.
I'm not sure to which extent we can solve this over the course of the next term, but I will endeavour to release at least quarterly updates
reporting on what we've spent money on, and also a summary of what we have available with our trusted organisations.
</P>


<P><B>3.2.2. More frequent updates.</B></P>

<P>It's been a fantastic trend of recent DPLs to generate monthly logs of their activities.
The problem with these are that they can get a bit long, which leads to important information potentially being missed.
By the time the monthly report is released, some information that may be more timely may also have gone stale.
</P>

<P>If elected, I will continue generating the monthly logs, but I intend to release more bite-sized updates in the interim,
which can then be linked back to the larger monthly report.
</P>

<H2> 4. Acknowledgements </H2>

<UL>
<LI>I used <a href="https://www.debian.org/vote/2010/platforms/zack">Zack's platform layout</a> as a base for this one.</LI>
</UL>

<H2> A. Rebuttals </H2>

<H3> A.1. Sruthi Chandran </H3>

<P>It's really great to have another woman run for DPL. I believe the last time that happened was in 2010,
and I consider it rather unfortunate that it's been so long. So, whatever happens, I want to thank her for
running and I'm sure she'll be an inspiration for many others.</P>

<P>In terms of her platform, her general intentions are good, but her platform lacks a lot of specifics.
This makes it more difficult to envision what a term with Sruthi would look like, it also makes it more
difficult to find areas that are either agreeable or not.</P>

<P>Her ideas about giving more attention to diversity spending could be interesting. There's been
some concern in recent discussions when it comes to return on investment on outreach spending and whether our
current outreach strategy is the optimal one. Personally I don't think anyone will ever reach an ROI of 1:1 for outreach,
that is, for every person you reach out to and invest in, you get one sustained contributor, but that
doesn't mean that outreach isn't worth while or that we shouldn't bother. Still, I'd be curious to see
what Sruthi would do with this.</P>

<P>I like the work she does as a DD, and she's in for a roller coaster of a ride organising a DebConf in
India in 2022, but I do feel that her overall exposure to the project could be a bit more solid. I checked my email
client for posts from her to debian-project, debian-vote and debian-devel to try to get some idea of the
type of posts she participates in and where she stands on issues, but it doesn't seem like she has participated
there much. I think the mere act of running for DPL and her immidiate future plans with organising a DebConf
will already help a lot in terms of building a profile within the Debian project, and if she's not elected,
then I hope she runs again in the future.</P>


<H3> A.2. Brian Gupta </H3>

<P>Brian's ideas about a foundation for Debian is compelling, and have brought some interesting discussion
to the forefront, and awareness of certain issues that might not have otherwise come to light.</P>

<P>However, I think his idea of turning a DPL vote in to a referendum isn't the best way to go about it.
A proper evaluation of our needs, a comparison of available options and further discussion that leads to
a GR would in my opinion lead to better outcomes.</P>

<P>Furthermore, I don't believe that it's actually possible to set up the kind of organisations that Brian
intends to in one DPL term. Simple questions like "Who will staff these organisations?",
"Will they earn a salary?", etc could be
really difficult to answer in the context of Debian. All of these could have good answers, but I think
it will take us some time to figure out exactly what we want, what we need, and how to match that up
to what's possible in a way that allows the project to thrive. Perhaps it might even be possible to
start a very small TO that just covers the delta of what the SPI and current TOs can currently provide
us compared to what we need, it's worth while exploring every possible option.</P>

<P>Starting one or more foundations could also have far-reaching consequences that we haven't begun to
consider yet. What if Brian is elected, puts in a year of work to get things going, then things don't work out,
and then he decides not to run for DPL  again. Will it be up to the rest of us to dismantle and fix everything?
Or will we be stuck with yet another Debian mess that we'll have to live with for years to come?</P>

<P>There are some things that we can rush, or even wing it as we go along, and there are some things
that should be done properly. Our organisational structure, backing strategy and project formalization deserves some proper
thought behind it, and I have no doubt that this will continue to be important within Debian for a while, but
I don't think that it should be grounded in a referendum, or to a specific DPL term.</P>


<H2> B. Changelog </H2>

<P> This platform is version controlled in a <a href="https://salsa.debian.org/jcc/dpl-platform">git repository.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/2.00">2.00</a>: New platform for 2020 DPL elections.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/2.00">2.01</a>: Add rebuttals.</LI>
</UL>

<BR>

</DIV> <!-- END MAIN -->



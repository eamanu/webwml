<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The package src:python-bottle before 0.12.19 are vulnerable to
Web Cache Poisoning by using a vector called parameter cloaking.</p>

<p>When the attacker can separate query parameters using a
semicolon (;), they can cause a difference in the interpretation
of the request between the proxy (running with default configuration)
and the server. This can result in malicious requests being cached
as completely safe ones, as the proxy would usually not see the
semicolon as a separator, and therefore would not include it in a
cache key of an unkeyed parameter.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.12.13-1+deb9u1.</p>

<p>We recommend that you upgrade your python-bottle packages.</p>

<p>For the detailed security status of python-bottle please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-bottle">https://security-tracker.debian.org/tracker/python-bottle</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2531.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>eterm, an enlightened terminal emulator, allowed (potentially
remote) code execution because of improper handling of certain
escape sequences (ESC G Q).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.6-5+deb9u1.</p>

<p>We recommend that you upgrade your eterm packages.</p>

<p>For the detailed security status of eterm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/eterm">https://security-tracker.debian.org/tracker/eterm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2681.data"
# $Id: $

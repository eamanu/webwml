<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of vulnerabilities in
<tt>libmediainfo</tt>, a library reading metadata such as track names, lengths,
etc. from media files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11372">CVE-2019-11372</a>

    <p>An out-of-bounds read in MediaInfoLib::File__Tags_Helper::Synched_Test
    in Tag/File__Tags.cpp in MediaInfoLib in MediaArea MediaInfo 18.12 leads to
    a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11373">CVE-2019-11373</a>

    <p>An out-of-bounds read in File__Analyze::Get_L8 in
    File__Analyze_Buffer.cpp in MediaInfoLib in MediaArea MediaInfo 18.12 leads
    to a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15395">CVE-2020-15395</a>

    <p>In MediaInfoLib in MediaArea MediaInfo 20.03, there is a stack-based
    buffer over-read in Streams_Fill_PerStream in Multiple/File_MpegPs.cpp (aka
    an off-by-one during MpegPs parsing).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26797">CVE-2020-26797</a>

    <p>Mediainfo before version 20.08 has a heap buffer overflow vulnerability
    via MediaInfoLib::File_Gxf::ChooseParser_ChannelGrouping.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.7.91-1+deb9u1.</p>

<p>We recommend that you upgrade your libmediainfo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2603.data"
# $Id: $

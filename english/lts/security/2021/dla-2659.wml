<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10196">CVE-2018-10196</a>

    <p>NULL pointer dereference vulnerability in the rebuild_vlists
    function in lib/dotgen/conc.c in the dotgen library allows
    remote attackers to cause a denial of service (application
    crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18032">CVE-2020-18032</a>

    <p>A buffer overflow was discovered in Graphviz, which could
    potentially result in the execution of arbitrary code when
    processing a malformed file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.38.0-17+deb9u1.</p>

<p>We recommend that you upgrade your graphviz packages.</p>

<p>For the detailed security status of graphviz please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/graphviz">https://security-tracker.debian.org/tracker/graphviz</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2659.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in openvswitch, a production quality,
multilayer, software-based, Ethernet virtual switch.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35498">CVE-2020-35498</a>

     <p>Denial of service attacks, in which crafted network packets
     could cause the packet lookup to ignore network header fields
     from layers 3 and 4. The crafted network packet is an ordinary
     IPv4 or IPv6 packet with Ethernet padding length above 255 bytes.
     This causes the packet sanity check to abort parsing header
     fields after layer 2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27827">CVE-2020-27827</a>

     <p>Denial of service attacks using crafted LLDP packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17206">CVE-2018-17206</a>

     <p>Buffer over-read issue during BUNDLE action decoding.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17204">CVE-2018-17204</a>

     <p>Assertion failure due to not validating information (group type
     and command) in OF1.5 decoder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9214">CVE-2017-9214</a>

     <p>Buffer over-read that is caused by an unsigned integer underflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8011">CVE-2015-8011</a>

     <p>Buffer overflow in the lldp_decode function in
     daemon/protocols/lldp.c in lldpd before 0.8.0 allows remote
     attackers to cause a denial of service (daemon crash) and
     possibly execute arbitrary code via vectors involving large
     management addresses and TLV boundaries.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.6.10-0+deb9u1. This version is a new upstream point release.</p>

<p>We recommend that you upgrade your openvswitch packages.</p>

<p>For the detailed security status of openvswitch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openvswitch">https://security-tracker.debian.org/tracker/openvswitch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2571.data"
# $Id: $

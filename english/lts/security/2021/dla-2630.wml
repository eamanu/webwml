<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29447">CVE-2021-29447</a>

    <p>Wordpress is an open source CMS. A user with the ability to
    upload files (like an Author) can exploit an XML parsing issue
    in the Media Library leading to XXE attacks. This requires
    WordPress installation to be using PHP 8. Access to internal
    files is possible in a successful XXE attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29450">CVE-2021-29450</a>

    <p>Wordpress is an open source CMS. One of the blocks in the
    WordPress editor can be exploited in a way that exposes
    password-protected posts and pages. This requires at least
    contributor privileges.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.7.20+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2630.data"
# $Id: $

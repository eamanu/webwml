<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Drupal project uses the pear Archive_Tar library, which has
released a security update that impacts Drupal.</p>

<p>The vulnerability is mitigated by the fact that Drupal core's use of
the Archive_Tar library is not vulnerable, as it does not permit
symlinks.</p>

<p>Exploitation may be possible if contrib or custom code uses the
library to extract tar archives (for example .tar, .tar.gz, .bz2, or
.tlz) which come from a potentially untrusted source.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
7.52-2+deb9u16.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>For the detailed security status of drupal7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/drupal7">https://security-tracker.debian.org/tracker/drupal7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>


<p>
<p>--u3/rZRmxL6MmkK24
Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2721.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that freeimage, a graphics library, was affected by the following
two security issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Heap buffer overflow caused by invalid memcpy in PluginTIFF. This flaw
    might be leveraged by remote attackers to trigger denial of service or any
    other unspecified impact via crafted TIFF data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Stack exhaustion caused by unwanted recursion in PluginTIFF. This flaw
    might be leveraged by remote attackers to trigger denial of service via
    crafted TIFF data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.15.4-4.2+deb8u2.</p>

<p>We recommend that you upgrade your freeimage packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2031.data"
# $Id: $

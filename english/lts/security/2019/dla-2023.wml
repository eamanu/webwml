<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, information disclosure or the execution
of arbitrary code.</p>

<p>Updates for the amd64 architecture are already available, new packages
for i386, armel and armhf will be available within the next 24 hours.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7u241-2.6.20-1~deb8u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2023.data"
# $Id: $

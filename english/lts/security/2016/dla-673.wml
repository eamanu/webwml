<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Roland Tapken discovered that insufficient input sanitizing in KMail's
plain text viewer allowed attackers the injection of HTML code. This
might open the way to the exploitation of other vulnerabilities in the
HTML viewer code, which is disabled by default.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4:4.8.4-2+deb7u1.</p>

<p>We recommend that you upgrade your kdepimlibs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-673.data"
# $Id: $

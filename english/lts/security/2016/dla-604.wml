<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in ruby-actionpack-3.2, a
web-flow and rendering framework and part of Rails:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7576">CVE-2015-7576</a>

  <p>A flaw was found in the way the Action Controller component compared
  user names and passwords when performing HTTP basic
  authentication. Time taken to compare strings could differ depending
  on input, possibly allowing a remote attacker to determine valid user
  names and passwords using a timing attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0751">CVE-2016-0751</a>

  <p>A flaw was found in the way the Action Pack component performed MIME
  type lookups. Since queries were cached in a global cache of MIME
  types, an attacker could use this flaw to grow the cache indefinitely,
  potentially resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0752">CVE-2016-0752</a>

  <p>A directory traversal flaw was found in the way the Action View
  component searched for templates for rendering. If an application
  passed untrusted input to the <q>render</q> method, a remote,
  unauthenticated attacker could use this flaw to render unexpected
  files and, possibly, execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2097">CVE-2016-2097</a>

  <p>Crafted requests to Action View might result in rendering files from
  arbitrary locations, including files beyond the application's view
  directory. This vulnerability is the result of an incomplete fix of
  <a href="https://security-tracker.debian.org/tracker/CVE-2016-0752">CVE-2016-0752</a>.  
  This bug was found by Jyoti Singh and Tobias Kraze
  from Makandra.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2098">CVE-2016-2098</a>

   <p>If a web applications does not properly sanitize user inputs, an
   attacker might control the arguments of the render method in a
   controller or a view, resulting in the possibility of executing
   arbitrary ruby code.  This bug was found by Tobias Kraze from
   Makandra and joernchen of Phenoelit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6316">CVE-2016-6316</a>

  <p>Andrew Carpenter of Critical Juncture discovered a cross-site
  scripting vulnerability affecting Action View. Text declared as <q>HTML
  safe</q> will not have quotes escaped when used as attribute values in
  tag helpers.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.6-6+deb7u3.</p>

<p>We recommend that you upgrade your ruby-actionpack-3.2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-604.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tony Finch and Marco Davids reported an assertion failure in BIND, a
DNS server implementation, which causes the server process to
terminate.  This denial-of-service vulnerability is related to a
defect in the processing of responses with DNAME records from
authoritative servers and primarily affects recursive resolvers.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u13.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-696.data"
# $Id: $

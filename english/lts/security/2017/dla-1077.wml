<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various security issues were discovered in faad2, a fast audio
decoder, that would allow remote attackers to cause a denial of
service (application crash due to memory failures or large CPU
consumption) via a crafted mp4 file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7-8+deb7u1.</p>

<p>We recommend that you upgrade your faad2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1077.data"
# $Id: $

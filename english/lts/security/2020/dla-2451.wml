<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libvncserver, an API to write one's own VNC
server.
Due to some missing checks, a divide by zero could happen, which could
result in a denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.9.11+dfsg-1.3~deb9u6.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>For the detailed security status of libvncserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvncserver">https://security-tracker.debian.org/tracker/libvncserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2451.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>opj_t1_clbl_decode_processor in openjp2/t1.c of OpenJPEG had a heap-based
buffer overflow in the qmfbid==1 case, a similar but different issue than
<a href="https://security-tracker.debian.org/tracker/CVE-2020-6851">CVE-2020-6851</a>.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.0-2+deb8u10.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2089.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various minor vulnerabilities have been addredd in libexif, a library to
parse EXIF metadata files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20030">CVE-2018-20030</a>

    <p>This issue had already been addressed via DLA-2214-1. However, upstream
    provided an updated patch, so this has been followed up on.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13112">CVE-2020-13112</a>

    <p>Several buffer over-reads in EXIF MakerNote handling could have lead
    to information disclosure and crashes. This issue is different from
    already resolved <a href="https://security-tracker.debian.org/tracker/CVE-2020-0093">CVE-2020-0093</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13113">CVE-2020-13113</a>

    <p>Use of uninitialized memory in EXIF Makernote handling could have
    lead to crashes and potential use-after-free conditions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13114">CVE-2020-13114</a>

    <p>An unrestricted size in handling Canon EXIF MakerNote data could have
    lead to consumption of large amounts of compute time for decoding
    EXIF data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.6.21-2+deb8u3.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2222.data"
# $Id: $

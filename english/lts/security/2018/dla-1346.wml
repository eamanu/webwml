<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A NULL Pointer Dereference was discovered in the TIFFPrintDirectory
function (tif_print.c) when using the tiffinfo tool to print crafted
TIFF information. This vulnerability could be leveraged by remote
attackers to cause a crash of the application.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u19.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1346.data"
# $Id: $

#use wml::debian::translation-check translation="04b2a712f0dc1f99d41ff1a394028d0309992b19" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans le code WPA. Ce dernier peut être
entrainé à reconfigurer les clefs WPA/WPA2/RSN (TK, GTK ou IGTK) en rejouant
une trame spécifique utilisée pour gérer les clefs. Une telle réinstallation de
la clef de chiffrement peut aboutir à deux types différents de vulnérabilités :
désactivation de la protection de relecture et réduction de manière
significative de la sécurité du chiffrement au point de permettre à des trames
d’être déchiffrées, ou identification des parties de clefs par un
attaquant en fonction du chiffrement utilisé.</p>

<p>Ces problèmes sont couramment connues sous l’appellation <q>KRACK</q>. Selon
US-CERT, « l’impact de l’exploitation de ces vulnérabilités inclut le
déchiffrement, le rejeu de paquet, le détournement de connexion TCP, l’injection
de contenu HTTP entre autres ».</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13077">CVE-2017-13077</a>

<p>Réinstallation de la clef de chiffrement « pairwise » (PTK-TK) dans
l’initialisation de connexion 4-way.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13078">CVE-2017-13078</a>

<p>Réinstallation de la clef de groupe (GTK) dans l’initialisation de connexion
4-way.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13079">CVE-2017-13079</a>

<p>Réinstallation de la clef d’intégrité de groupe (IGTK) dans l’initialisation
de connexion 4-way.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a>

<p>Réinstallation de la clef de groupe (GTK) dans l’initialisation de connexion
de clef de groupe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13081">CVE-2017-13081</a>

<p>Réinstallation de la clef d’intégrité de groupe (IGTK) dans l’initialisation
de connexion de clef de groupe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13082">CVE-2017-13082</a>

<p>Acception d’une requête retransmise de réassociation Fast BSS Transition (FT)
et réinstallation de la clef de chiffrement « pairwise » (PTK-TK) lors de son
traitement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13084">CVE-2017-13084</a>

<p>Réinstallation de la clef STK dans l’initialisation de connexion PeerKey.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13086">CVE-2017-13086</a>

<p>Réinstallation de la clef Peerkey (TPK) Tunneled Direct-Link Setup (TDLS)
dans l’initialisation de connexion TDLS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13087">CVE-2017-13087</a>

<p>Réinstallation de la clef de groupe (GTK) lors du traitement d’une trame
de réponse du mode « sleep » Wireless Network Management (WNM).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13088">CVE-2017-13088</a>

<p>Réinstallation de la clef d’intégrité de groupe (IGTK) lors du traitement
d’une trame de réponse du mode « sleep » Wireless Network Management (WNM).</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0-3+deb7u5. Remarquez que les deux dernières vulnérabilités
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-13087">CVE-2017-13087</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2017-13088">CVE-2017-13088</a>)
étaient de manière fautive marquées comme corrigées dans le journal de
modifications alors qu’elles ne s’appliquaient pas à la version 1.0 du code
source WPA, qui n’implémentent pas les réponses du mode « sleep » WNM.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1150.data"
# $Id: $

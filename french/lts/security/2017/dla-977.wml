#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans FreeRADIUS, un serveur RADIUS de
haute performance et grandement configurable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2015">CVE-2014-2015</a>

<p>Une dépassement de pile a été découvert dans la fonction normify dans le
module rlm_pap qui peut être attaqué par des utilisateurs pour provoquer un
déni de service ou d’autres problèmes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4680">CVE-2015-4680</a>

<p>Freeradius échouait à la vérification de révocation de certificats CA
intermédiaires, par conséquent acceptant des certificats de client issus de
certificats révoqués de CA intermédiaires.</p>

<p>Remarquez que pour activer la vérification de certificats intermédiaires, il
est nécessaire d’activer l’option check_all_crl de la section TLS EAP dans
eap.conf. Cela est seulement nécessaire pour les serveurs utilisant les
certificats signés par des CA intermédiaires. Les serveurs utilisant des CA
auto-signés ne sont pas touchés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9148">CVE-2017-9148</a>

<p>Le cache de session TLS échoue à prévenir de manière fiable la reprise de
session non authentifiée. Cela permet à des attaquants distants (tels que des
solliciteurs 802.1X malveillants) de contourner l’authentification  à l'aide
de PEAP ou TTLS.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.1.12+dfsg-1.2+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freeradius.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-977.data"
# $Id: $

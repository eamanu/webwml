#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans SimpleSAMLphp, un
cadriciel d'authentification, principalement au moyen du protocole SAML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9814">CVE-2016-9814</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9955">CVE-2016-9955</a>

<p>Une vérification incorrecte des valeurs de retour dans les utilitaires
de validation de signature permettait à un attaquant de faire accepter des
signatures non valables comme valables dans le rare cas d'une erreur
survenant pendant la validation.</p></li>

<li>SSPSA-201802-01 (pas encore de CVE)

<p>Vulnérabilité critique de validation de signature.</p></li>

</ul>

<p>En complément, cette mise à jour ajoute un correctif pour résoudre une
consommation excessive de ressource dans le cas du traitement d'un grand
fichier de métadonnées par SimpleSAMLphp.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.9.2-1+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets simplesamlphp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1297.data"
# $Id: $

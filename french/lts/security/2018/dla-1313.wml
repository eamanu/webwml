#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans les client, relais et
serveur.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5732">CVE-2018-5732</a>

<p>Felix Wilhelm de l'équipe de sécurité de Google a découvert que le client
DHCP est prédisposé à une vulnérabilité d’accès mémoire hors limites lors du
traitement de réponses d’options DHCP spécialement construites, aboutissant
à une exécution potentielle de code arbitraire par un serveur DHCP malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5733">CVE-2018-5733</a>

<p>Felix Wilhelm de l'équipe de sécurité de Google a découvert que le serveur
DHCP ne gère pas correctement le comptage de références lors du traitement de
requêtes de client. Un client malveillant peut exploiter ce défaut pour
provoquer un déni de service (plantage de dhcpd) en envoyant une grande
quantité de trafic.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.2.2.dfsg.1-5+deb70u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets isc-dhcp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1313.data"
# $Id: $

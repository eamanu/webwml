#use wml::debian::translation-check translation="3915d92acfbaf77851b3d249cbc900cf19aabd0f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur de courriels
Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">CVE-2020-12100</a>

<p>La réception d’un courriel avec des parties MIME imbriquées profondément
conduisait à un épuisement des ressources lorsque Dovecot essayait de
l’analyser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12673">CVE-2020-12673</a>

<p>L’implémentation NTLM de Dovecot ne vérifiait pas correctement la taille de
tampon de message. Cela conduisait à un plantage lors de la lecture de
l’allocation antérieure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12674">CVE-2020-12674</a>

<p>L’implémentation du mécanisme RPA de Dovecot acceptait les messages de
longueur nulle. Cela conduisait à un plantage d’assertion.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.2.27-3+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dovecot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dovecot">https://security-tracker.debian.org/tracker/dovecot</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2328.data"
# $Id: $

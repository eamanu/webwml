#use wml::debian::translation-check translation="5760135f984834fada6d66960163893f950ff71b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans netty, un cadriciel
client/serveur NIO de socket en Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

<p>HttpObjectDecoder.java dans Netty avant la version 4.1.44 permet un en-tête
HTTP auquel manque un deux-points, qui pourrait être interprété comme un en-tête
indépendant avec une syntaxe incorrecte, ou qui pourrait être interprété comme un
«invalid fold».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

<p>HttpObjectDecoder.java dans Netty avant la version 4.1.44 permet un en-tête
Content-Length accompagné par un autre en-tête Content-Length, ou par un en-tête
Transfer-Encoding.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7238">CVE-2020-7238</a>

<p>4.1.43.Final de Netty permet une dissimulation de requête HTTP à cause d’une
mauvaise gestion d’espace blanc de Transfer-Encoding (tel qu’une ligne 
[espace]Transfer-Encoding:chunked) et d'un en-tête Content-Length suivant. Ce
problème existe à cause d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11612">CVE-2020-11612</a>

<p>Les décodeurs Zlib dans Netty 4.1.x avant la version 4.1.46 permettent une
allocation de mémoire illimitée lors du décodage de flux d’octets Zlib.
Un attaquant pourrait envoyer un grand flux d’octets encodés Zlib au serveur
Netty, obligeant le serveur à allouer toute sa mémoire libre à un seul décodeur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:4.1.7-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netty.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netty, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/netty">https://security-tracker.debian.org/tracker/netty</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2364.data"
# $Id: $

#use wml::debian::translation-check translation="cd0bf8b46e55223d74a3837b6bf7fc8d93156c9a" maintainer=""
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21159">CVE-2021-21159</a>

<p>Khalil Zhani a découvert un problème de dépassement de tampon dans
l'implémentation de Tab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21160">CVE-2021-21160</a>

<p>Marcin Noga a découvert un problème de dépassement de tampon dans
WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21161">CVE-2021-21161</a>

<p>Khalil Zhani a découvert un problème de dépassement de tampon dans
l'implémentation de Tab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21162">CVE-2021-21162</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21163">CVE-2021-21163</a>

<p>Alison Huffman a découvert un problème de validation de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21165">CVE-2021-21165</a>

<p>Alison Huffman a découvert une erreur dans l'implémentation audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21166">CVE-2021-21166</a>

<p>Alison Huffman a découvert une erreur dans l'implémentation audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21167">CVE-2021-21167</a>

<p>Leecraso et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération dans l'implémentation des marque-pages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21168">CVE-2021-21168</a>

<p>Luan Herrera a découvert une erreur d'application de politique dans
appcache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21169">CVE-2021-21169</a>

<p>Bohan Liu et Moon Liang ont découvert un problème d'accès hors limites
dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21170">CVE-2021-21170</a>

<p>David Erceg a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21171">CVE-2021-21171</a>

<p>Irvan Kurniawan a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21172">CVE-2021-21172</a>

<p>Maciej Pulikowski a découvert une erreur d'application de politique dans
l'API File System Access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21173">CVE-2021-21173</a>

<p>Tom Van Goethem a découvert une fuite d'informations basée sur le réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21174">CVE-2021-21174</a>

<p>Ashish Guatam Kambled a découvert une erreur d'implémentation dans le
« Referrer Policy ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21175">CVE-2021-21175</a>

<p>Jun Kokatsu a découvert une erreur d'implémentation dans la fonction
« isolation des sites ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21176">CVE-2021-21176</a>

<p>Luan Herrera a découvert une erreur d'implémentation dans le mode plein
écran.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21177">CVE-2021-21177</a>

<p>Abdulrahman Alqabandi a découvert une erreur d'application de politique
dans la fonctionnalité Autofill.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21178">CVE-2021-21178</a>

<p>Japong a découvert une erreur dans l'implémentation du compositeur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21179">CVE-2021-21179</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans l'implémentation du réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21180">CVE-2021-21180</a>

<p>Abdulrahman Alqabandi a découvert un problème d'utilisation de mémoire
après libération dans la fonctionnalité de recherche d'onglet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21181">CVE-2021-21181</a>

<p>Xu Lin, Panagiotis Ilias et Jason Polakis ont découvert une fuite
d'informations par canal auxiliaire dans la fonctionnalité Autofill.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21182">CVE-2021-21182</a>

<p>Luan Herrera a découvert une erreur d'application de politique dans
l'implémentation de la navigation de site.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21183">CVE-2021-21183</a>

<p>Takashi Yoneuchi a découvert une erreur d'implémentation dans l'API
Performance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21184">CVE-2021-21184</a>

<p>James Hartig a découvert une erreur d'implémentation dans l'API
Performance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21185">CVE-2021-21185</a>

<p>David Erceg a découvert une erreur d'application de politique dans les
extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21186">CVE-2021-21186</a>

<p>dhirajkumarnifty a découvert une erreur d'application de politique dans
l'implémentation de la lecture de QR code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21187">CVE-2021-21187</a>

<p>Kirtikumar Anandrao Ramchandani a découvert une erreur de validation de
données dans le formatage des URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21188">CVE-2021-21188</a>

<p>Woojin Oh a découvert un problème d'utilisation de mémoire après
libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21189">CVE-2021-21189</a>

<p>Khalil Zhani a découvert une erreur d'application de politique dans
l'implémentation des paiements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21190">CVE-2021-21190</a>

<p>Zhou Aiting a découvert une utilisation de mémoire non initialisée dans
la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21191">CVE-2021-21191</a>

<p>raven a découvert un problème d'utilisation de mémoire après libération
dans l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21192">CVE-2021-21192</a>

<p>Abdulrahman Alqabandi a découvert un problème de dépassement de tampon
dans l'implémentation de Tab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21193">CVE-2021-21193</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21194">CVE-2021-21194</a>

<p>Leecraso et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération dans la fonctionnalité de capture d'écran.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21195">CVE-2021-21195</a>

<p>Liu et Liang ont découvert un problème d'utilisation de mémoire après
libération dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21196">CVE-2021-21196</a>

<p>Khalil Zhani a découvert un problème de dépassement de tampon dans
l'implémentation de Tab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21197">CVE-2021-21197</a>

 <p>Abdulrahman Alqabandi a découvert un problème de dépassement de tampon
dans l'implémentation de Tab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21198">CVE-2021-21198</a>

<p>Mark Brand a découvert un problème de lecture hors limites dans
l'implémentation de la communication inter-processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21199">CVE-2021-21199</a>

<p>Weipeng Jiang a découvert un problème d'utilisation de mémoire après
libération dans le gestionnaire de fenêtre et d'événement Aura.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 89.0.4389.114-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4886.data"
# $Id: $

#use wml::debian::template title="Programme pour Martin Michlmayr" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="cae583a87602d42ff8b184e925304aacb08c788d" maintainer="Thomas Vincent"

<h2>Introduction</h2>

<p>

Je vais commencer par un aveu. Depuis maintenant quelques années, je pense
qu'il est temps pour moi de quitter le projet Debian. Cela vient en partie
du fait que mes centres d'intérêt ont changé au fil des ans, mais aussi
en partie parce que… Debian n'est plus aussi excitante qu'auparavant. La
seule raison pour laquelle je n'ai pas encore démissionné est que Debian et
sa communauté jouent un rôle si important dans ma vie que je ne peux pas
lâcher prise.

</p>

<p>

Je pense que mes sentiments partagés au sujet de Debian ne sont pas isolés.
Nous avons vu nombre de contributeurs reconnus quitter le projet ces derniers
temps, par frustration ou ennui, et je pense que nombreuses sont les personnes
qui restent mais qui sont déjà parties mentalement.

</p>

<p>

Voyant qu'il n'y avait pas de candidat pour le rôle de responsable du projet
Debian (DPL), ce que je considère comme un symptôme de graves problèmes
sous-jacents, m'a fait réfléchir. Comme chaque personne faisant le choix de
contribuer à Debian, j'ai deux possibilités : je peux démissionner, partir
frustré, ou je peux aider à discuter, confronter, attaquer et
résoudre ces problèmes.

</p>

<h2>Debian aujourd'hui</h2>

<p>

Debian semble se trouver dans une situation très particulière actuellement.
Il y a environ 10 ou 15 ans, Debian vivait une crise existentielle. Un
certain nombre de facteurs ont contribué à rendre Debian moins pertinente.
Cela comprend l'arrivée d'Ubuntu (qui proposait un environnement de bureau
bien mieux fini à cette époque et la prise en charge à long terme pour les
serveurs), le mouvement vers MacOS d'Apple (dans le grand public voire même
parmi les développeurs de logiciel libre, en particulier dans le domaine du
web), et un mouvement général vers les appareils mobiles.

</p>

<p>

Aujourd'hui, les choses sont très différentes. Debian est plus
populaire que jamais sur serveurs. Il y a une compréhension renouvelée de
l'importance de Debian et une nouvelle vague d'adoption dans le domaine
du cloud et des conteneurs où un système d'exploitation stable est essentiel.
Debian est partout. Elle fait fonctionner de grandes parties de
l'infrastructure informatique de notre société et constitue les briques de
base de bien d'autres solutions. Les utilisateurs et les entreprises
n'apprécient pas uniquement la qualité du système Debian, mais aussi la maturité
de notre communauté et la nature ouverte de notre processus de développement
qui n'est pas dominé par une seule entité.

</p>

<p>

Alors, malgré ce succès et cette importance, pourquoi semble-t-il parfois
que Debian soit devenue quelque peu non pertinente ? Pourquoi n'y a-t-il pas
de battage autour de Debian qui reflète son succès dans tant de domaines ?

</p>

<h2>Problèmes et solutions</h2>

<p>

J'ai récemment lu l'article de Michael Stapelberg à propos de la <a href =
"https://michael.stapelberg.ch/posts/2019-03-10-debian-winding-down/">réduction
de son investissement dans Debian</a> dans lequel il décrit à quel point
l'infrastructure et les processus de Debian sont devenus stagnants. Son
article très réfléchi résonne en moi, bien que je pense que les problèmes sont
bien plus importants que cela.

</p>

<p>

Le monde du logiciel libre a fondamentalement changé de bien des façons
au cours des 5 à 10 dernières années. Pourtant, si vous regardez Debian,
nous opérons globalement de la même façon qu'il y a 20 ans. Debian était
une pionnière, une vraie meneuse. Les gestionnaires de paquets, les mises
à jour automatiques et les constructions de paquets sur plus de 10 architectures
étaient de vraies innovations à l'époque. La seule innovation significative
à laquelle je peux penser qui ait émergé de Debian récemment est le mouvement
de compilation reproductible. Les compilations reproductibles résolvent
un problème important et cette idée s'est répandue au-delà de Debian dans
tout le monde du libre.

</p>

<p>

Je déteste quand les grandes entreprises parlent d'être réactives ou d'autres
termes à la mode. Mais en regardant Debian, je comprends finalement ce
qu'elles veulent dire &mdash; le projet a évolué d'une façon qui rend le
changement difficile. Nous avons échoué à nous adapter au nouvel environnement
dans lequel nous nous trouvons et nous avons du mal à suivre le rythme d'un
monde qui change toujours plus rapidement.

</p>

<h3>Durabilité</h3>

<p>

Quand je me suis impliqué dans les logiciels libres dans les années 1990,
c'était par curiosité technique et par passion pour la liberté logicielle.
Il s'agissait d'un loisir et je ne pensais pas bâtir une carrière dessus.
Mais de plus en plus de sociétés ont découvert la supériorité technique du
logiciel développé en suivant les modèles collaboratifs du logiciel libre
associé aux libertés pratiques offertes par le logiciel libre, et l'industrie
a décollé. Beaucoup d'entre nous ont aujourd'hui la chance de ne pas
seulement contribuer au logiciel libre sur notre temps libre mais comme
une partie de notre activité professionnelle.

</p>

<p>

Aujourd'hui, la majorité des projets libres ayant du succès reposent sur
(et prospèrent grâce à !) des contributeurs payés. Ce n'est pas un échec,
mais une réflexion du succès du logiciel libre. Malheureusement, comparé
à d'autres projets couronnés de succès, la proportion de contributeurs
payés est bien plus faible dans Debian et je vois cela comme un échec.

</p>

<p>

Au fil des années, j'ai vu de nombreux contributeurs de valeur quitter
Debian après avoir obtenu leur diplôme universitaire et trouvé un emploi
(souvent dans le logiciel libre, mais pas dans Debian, ou travaillant <em>avec</em>
Debian, mais pas <em>sur</em> Debian). J'ai vu de nombreux contributeurs
dévoués qui avaient deux emplois &mdash; un emploi payé
le jour, et un boulot non payé pour Debian les soirs et week-ends. J'ai
rencontré de nombreux contributeurs qui ont fait de grands sacrifices pour
contribuer à Debian. Malheureusement, j'ai aussi vu beaucoup de burn-outs
(et j'en ai moi-même fait l'expérience) !

</p>

<p>

Si vous voulez travailler sur Debian en tant que loisir, super ! Mais si
vous voulez faire de Debian votre carrière, vous devriez pouvoir le faire.
Bien qu'il y ait quelques opportunités payées autour de Debian, je crois
qu'elles sont actuellement trop rares et que nous pouvons prendre un
certain nombre d'actions pour que plus de contributeurs puissent faire carrière
avec Debian.

</p>

<p>

Tout d'abord, nous pouvons aider davantage de sociétés à s'impliquer dans
Debian. Cela ne veut pas dire que nous allons sacrifier notre neutralité
ou notre attention à la qualité. Cela veut dire que nous allons embarquer
plus de contributeurs qui vont travailler à améliorer Debian pour tout le
monde. Nous avons déjà un certain nombre de sociétés (Arm, credativ, Google,
pour n'en nommer que quelques-unes) qui font des investissements
stratégiques dans Debian et paient des ingénieurs pour travailler sur
Debian.

</p>

<p>

Nous pouvons encourager plus de sociétés à s'impliquer. Nous pouvons
leur simplifier la tâche pour contribuer, nous pouvons montrer aux
sociétés qui se basent sur Debian l'importance de s'engager activement
(et les aider à développer le dossier commercial pour cela) et nous
pouvons fournir plus d'incitations, comme par exemple mieux reconnaître
leurs contributions.

</p>

<p>

Deuxièmement, les subventions deviennent un moyen populaire de financer les
projets de R&amp;D et le développement de logiciel libre. Le projet des
compilations reproductibles est financé par des subventions de la
fondation Linux et d'autres organisations. Je doute qu'il ait pu
accomplir autant en si peu de temps sans ce financement. Nous avons
beaucoup de personnes intelligentes dans Debian avec de bonnes idées
&mdash; le DPL peut identifier des subventions et les aider à postuler
pour se faire financer.

</p>

<p>

Troisièmement, nous devons travailler avec toute la communauté du logiciel
libre pour trouver plus de moyens durables de financer le développement
du logiciel libre. La débâcle d'OpenSSL a montré que la société dépend
du logiciel libre, sans faire les investissements qui s'imposent. De nombreux
développeurs indépendants de logiciel libre vivent très difficilement de
leur travail. Des plateformes comme Patreon existent mais très peu peuvent
en vivre car il n'y a pas assez de sensibilisation sur le fait que les gens
doivent investir et sponsoriser le logiciel libre.

</p>

<p>

Je suis sûr qu'il existe d'autres moyens. Dans l'ensemble, je pense qu'il
va devenir de plus en plus difficile pour des projets importants sans
contributeurs rémunérés d'entrer en compétition avec des initiatives dont
tous les contributeurs sont payés à plein temps et je pense que cela explique
partiellement pourquoi Debian a du mal à suivre le rythme. Plus
fondamentalement, nous devons également nous demander quel type de projet
nous souhaitons être. Le monde autour de nous a changé et il est temps
de l'admettre.

</p>

<p>

L'effort de prise en charge à long terme (LTS) organisé par Raphaël Hertzog
de Freexian est un succès spectaculaire. Les compilations reproductibles
ont attiré un financement significatif pour améliorer un aspect important de
Debian. Est-il temps d'intégrer ces efforts et de les poursuivre en tant que
Debian ? Ou devrions-nous encourager plus d'initiatives similaires autour de
Debian et leur donner un coup de main ? Est-il temps d'utiliser nos finances
pour certaines choses ?

</p>

<p>

Ce sont des questions importantes auxquelles nous, en tant que projet,
devrons apporter des réponses tôt ou tard. Cependant, nous pouvons
commencer par inviter plus d'entreprises à participer à notre communauté
et encourager celles qui dépendent de Debian à contribuer.

</p>

<h3>Direction et culture</h3>

<p>

Je vois un manque de direction dans Debian. Je n'entends pas par là seulement
une direction du type DPL, mais un manque de direction en général. La beauté
du logiciel libre est que tout le monde peut contribuer. Vous n'avez pas
besoin d'une permission pour devenir un meneur. Si vous avez une excellente
idée, réalisez-la !

</p>

<p>

Malheureusement, bien que nous ayons beaucoup de personnes talentueuses au
sein de Debian, je pense que nous avons atteint un point où les gens ont
peur de faire des changements ou d'en proposer, en particulier les changements
de grande ampleur. Ils ont peur des <q>flame-wars</q> qui en découleront ou
d'autres retombées (ou sont simplement fatigués de devoir lutter contre la
résistance au changement).

</p>

<p>

Pourquoi notre communauté fait-t-elle si peur à s'exprimer ? Pourquoi
avons-nous stagné ? Il me semble que Debian a développé un certain nombre
d'anti-patterns toxiques au fil des ans et dont nous devons nous éloigner.

</p>

<p>

Ce que je vais maintenant dire sera très controversé. Debian tire une grande
fierté d'être le système d'exploitation universel. Il s'agit d'un grand
objectif, à plusieurs niveaux. Cependant, il est aussi important d'admettre
que vous ne pouvez pas tout faire &mdash; que tout faire ralentit les choses.
Nous ne pouvons pas toujours attendre tout le monde, autrement nous
n'accomplirons jamais rien. Tout n'est pas d'égale importance. Parfois, il est
important de fixer les priorités et de dire « non » aux gens. À nouveau, nous
avons peur de cela. Le système d'exploitation universel, et la culture
sous-jacente, bien qu'ils soient louables en théorie, sont devenus toxiques
en pratique à plusieurs niveaux.

</p>

<p>

Il en va de même de l'excellence technique, un autre objectif louable, qui
devient toxique quand il est poussé à l'extrême. Nous parlons, parlons…
et parlons encore. Nous devrions nous rappeler que toutes les batailles ne
valent pas qu'on les mène et tout argument n'a pas besoin d'être discuté
jusqu'au moindre détail. Ce n'est pas grave de ne pas tomber d'accord, parfois. Nous
devons nous rappeler de faire une pause, réfléchir et demander : Est-ce que
ça vaut vraiment le coup ? Ai-je vraiment besoin d'envoyer ce courriel ?
Quel en sera le coût social ? Arrêtons d'épuiser notre communauté ! Nous
ne sommes pas un club de débat. Nous sommes là pour résoudre des problèmes
de façon pratique et pour délivrer les solutions à nos utilisateurs.

</p>

<p>

En plus de davantage de meneurs, il nous faut plus de soutiens (<q>cheerleaders</q>).
Nous devons remercier les gens. Nous devons apprécier les contributions.
Nous devons faire parler de Debian. Nous devrions célébrer notre succès et
bâtir sur celui-ci. (Et oui, j'apprécie l'ironie de dire cela dans un
programme qui consiste majoritairement en une critique de Debian. Cependant,
en tant que DPL et membre de la communauté en général, je compte dire
« Merci » plus souvent et encourager d'autres personnes à faire de même.)

</p>

<h3>Influence</h3>

<p>

Debian joue un rôle très spécial et important dans l'écosystème du
logiciel libre. Nous sommes respectés et nos contributions sont
appréciées. Les contributeurs Debian ont tendance à être des meneurs
dans l'espace du libre. Nous sommes fiers de ne pas seulement
empaqueter des logiciels venant de l'amont mais aussi de maintenir
de bonnes relations. Cela aboutit souvent à notre implication en
amont et à y prendre des rôles de dirigeant. Vous pouvez aussi
regarder les membres actuels et passés du conseil de l'Open Source
Initiative (OSI) et vous y verrez beaucoup de membres de Debian.

</p>

<p>

Bien que les membres de Debian jouent des rôles importants partout, ils
ne représentent souvent pas le projet Debian. Nous devons apprendre à
parler d'une seule voix. Dans l'ensemble, je pense que nous, en tant que
projet, devons plus nous faire entendre et prendre un rôle plus actif
pour influencer l'écosystème du libre. Debian a une réputation incroyable
mais nous n'utilisons pas notre influence pour les changements importants.

</p>

<p>

Bien que nous soyons plutôt bons pour influencer les aspects techniques,
nous avons une faible emprise sur le monde de l'entreprise dans le libre.
Et soyons honnêtes &mdash; l'open source est devenu une affaire très
importante. Nous manquons des opportunités importantes parce que nous jouons
mal à ce jeu. Nous devons prendre une place à la table (et oui, il s'agit
souvent d'une vraie table ou d'une conférence téléphonique, pas d'un
courriel).

</p>

<h2>Contexte</h2>

<p><img src="tbm.jpg" alt="Photo of Martin Michlmayr" align="right"></p>

<p>

Qui suis-je et comment vois-je le rôle de DPL ? Je suis Développeur Debian
depuis 2000 et j'ai travaillé dans un grand nombre de domaines au fil des
ans. J'ai travaillé comme responsable de candidature, aidant les nouveaux
contributeurs à rejoindre le projet et j'ai aussi géré le secrétariat des
nouveaux membres. J'ai contribué à divers efforts d'assurance qualité, j’ai
rempli des rapports de bogues et j'ai introduit le processus de membre
manquant à l'appel (MIA). J'ai porté Debian sur divers appareils ARM et
MIPS et j'ai écrit de la documentation pour ça. J'ai servi en tant que
responsable du projet Debian de 2003 à 2005. La plupart de mes contributions
récentes à Debian se sont faites à travers Software in the Public Interest,
une organisation de confiance de Debian.

</p>

<p>

J'ai travaillé pour Hewlett Packard pendant neuf ans à faciliter et conduire
diverses activités internes et externes en rapport avec le libre. J'ai été
membre du conseil de l'Open Source Initiative pendant six ans et membre du
conseil de Software in the Public Interest pendant près de cinq ans. Je suis
actuellement membre du conseil de Software Freedom Conservancy, qui accueille
le projet Debian Copyright Aggregation. Au fil des ans, je suis devenu un
geek de la comptabilité et je contribue à ledger, beancount et ledger2beancount.

</p>

<p>

Les opinions divergent à propos de l'engagement en temps que demande le
statut de DPL, allant de propositions pour une Debian « sans leader »
jusqu'à l'idée qu'être DPL est un emploi à plein temps. En me basant sur
mon expérience d'ancien responsable du projet, je suis fermement du deuxième
avis. Quand j'étais DPL, j'étais étudiant et j'avais du temps à revendre
(c'est ce que j'en pense maintenant). Je suis maintenant consultant en
logiciel libre et chaque heure que je passerais en tant que DPL m'éloignerait
de ce travail. Je suis prêt, néanmoins, à consacrer un temps significatif
à Debian (Il se peut aussi que je mette en place une cagnotte virtuelle).

</p>

<p>

Le rôle de DPL est vraiment unique. Dans Debian, vous n'êtes pas « promu »
au sens traditionnel du terme. Vous commencez à faire le travail et les
gens finissent par vous reconnaître pour ça. Tout est basé sur la
réputation et idéalement nous voulons un responsable du projet qui ait
déjà gagné sa réputation de meneur. J'ai donné une présentation au sujet
de la culture du libre lors d’un événement il y a quelques temps et j'ai
insisté sur l'importance de se construire une bonne réputation. Pour
illustrer cela, j'ai dit qu'il existe une personne particulière dans Debian
et que quand cette personne envoie un courriel, les gens l'écoutent car
ils savent que ce courriel contient un écrit de réel bon sens exprimé
très calmement. Bien que Debian ait plus d'un millier de contributeurs
<em>incroyables</em>, cette description était suffisante pour que certains
Développeurs Debian dans l'assistance hochent la tête en comprenant tous
de qui il s'agissait (sans que le nom de la personne ne soit mentionné).
C'est le genre de réputation que vous voulez bâtir ! (Et il y a bien
d'autres contributeurs que je pourrais décrire par leur façon unique de
contribuer à Debian !).

</p>

<p>

Malheureusement, je n'ai pas été actif dans Debian dernièrement et je n'ai
donc pas la réputation que j'attendrais d'un responsable du projet Debian.
Bien que beaucoup de contributeurs de longue date soient familiers de mon
travail, je suis une toile blanche pour beaucoup de nouveaux contributeurs.
Je suis conscient de cela, et si je suis élu, je travaillerai durement pour
gagner leur confiance.

</p>

<h2>Le responsable du projet Debian</h2>

<p>

Le rôle du DPL est souvent décrit comme ayant une fonction interne et
externe et les deux sont absolument vitales. Je crois que vous pouvez aussi
définir trois rôles pour le DPL : un administrateur, un facilitateur et un
meneur.

</p>

<p>

Bien que j'apprécie de mener à bien des tâches, je trouve qu'il est également
très gratifiant d'aider les autres à accomplir des choses. De cette façon, je
vois le DPL comme un facilitateur. Je viens d'assister à FOSSASIA où
quelqu'un a comparé la gestion de communauté à la conduite d'un orchestre.
Quand vous regardez les chefs d'orchestre, vous pouvez vous demander ce qu'ils
font à part bouger les mains en l'air. Vous ne voyez pas toute la coordination
et la pratique nécessaires à une exécution impeccable. Des DPL ont été
critiqués de ne pas avoir été actifs par le passé en particulier parce que
vous ne voyez pas une grande partie du travail quotidien. Vous ne voyez que
si quelque chose n'est pas fait ou se passe mal !

</p>

<p>

La facilitation comprend beaucoup de coordination. Cela implique d'aider
les gens à faire leur travail efficacement, faire disparaître les problèmes,
utiliser les délégations et faire du suivi. Cela implique d'écouter les
personnes. Cela implique aussi de demander aux gens de réaliser certaines
tâches. Vous seriez surpris de voir à quel point il est efficace de demander
de l'aide à quelqu'un. Si vous ne demandez pas, rien n'est fait. La
facilitation n'a pas à être <em>réactive</em> comme elle l'a souvent été.
J'aimerais être proactif sur la résolution des problèmes et l'identification
des domaines qui doivent être traités.

</p>

<p>

Le rôle de meneur externe est aussi important. Il y a des discussions à
propos du remplacement du responsable du projet par un conseil ou un
comité, et bien qu'il soit important d'avoir ces discussions et de faire
évoluer notre gouvernance, je pense que nous ne devrions pas ignorer
divers facteurs humains. Les sociétés, la presse et d'autres veulent
souvent parler au responsable du projet. Ils ne veulent pas s'adresser
à une liste de diffusion, ou à un comité et parfois même pas un
représentant. En tant que DPL, je compte travailler avec des
représentants pour former plus de connexions et de partenariats,
construire des ponts avec nos alliés. J'aimerais aussi lancer une
conversation honnête à propos de l'état de Debian. Comment pouvons-nous
améliorer notre culture ? Qu'est-ce qui nous freine ?

</p>

<p>

Enfin, il est important de prendre du recul et regarder l'image d'ensemble.
Ce programme est très critique envers Debian de plusieurs façons. Je devrais
dire que je suis très critique de nature et que je tends à insister sur
le côte négatif. Heureusement, je suis capable d'utiliser ce défaut de
personnalité de façons positives, comme dans mon travail d'assurance qualité
où j'identifie des bogues et que ce retour est utilisé pour améliorer le
logiciel. Mais il est important de mettre en avant les bons aspects. Debian
est si unique, si merveilleuse et si spéciale de tellement de façons. Si je
ne croyais pas en Debian, je ne serais pas candidat à cette élection. J'ai
été DPL auparavant et je sais comme cela peut parfois être difficile.
Oui, je vois des problèmes sévères dans Debian, mais je crois fermement que
nous pouvons les résoudre ensemble !

</p>

<h2>Réfutations</h2>

<h3>Joerg Jaspert</h3>

<p>

Joerg décrit un certain nombre de questions importantes pour Debian et le DPL.
Je suis d'accord avec son argument qu'il est important d'écouter les utilisateurs,
y compris les anciens utilisateurs qui ont quitté Debian. Bien que je sois
globalement d'accord avec son programme, je le trouve un peu « générique ». Ce
qu'il compte accomplir n'est pas clair, de même que ce qui le différencierait
des autres candidats. Néanmoins, comme le souligne Joerg, chaque DPL a son propre
style, et au bout du compte, nous élisons une personne et non un programme.

</p>

<h3>Jonathan Carter</h3>

<p>

Jonathan soulève un nombre de questions importantes dans son programme. Je suis
absolument d'accord qu'il est important d'améliorer les processus communautaires
et de prendre soin de notre communauté et que réparer notre communication est une
étape importante pour régler un grand nombre de problèmes dans le projet. Je suis
aussi d'accord avec sa vision du DPL comme un facilitateur et quelqu'un
d'accessible (de mon point de vue, à la fois au sein du projet et dans les autres
parties de la communauté).

</p>

<p>

Normalement, je serais d'accord avec l'idée qu'il est important de réaliser des
modifications petites et incrémentales, mais je pense que Debian a atteint un point
où il est important de repenser en profondeur comment notre communauté fonctionne.

</p>

<h3>Sam Hartman</h3>

<p>

J'aime le programme de Sam et, même s'ils diffèrent de bien des façons, je vois
beaucoup de parallèles entre nos programmes. Il décrit avec beaucoup d'éloquence
que « le succès dans une communauté n’est pas seulement technique mais aussi
émotionnel » et que Debian peut parfois être épuisante. Cela est similaire aux
anti-patterns toxiques que je décris et qui peuvent conduire les contributeurs à
résister, décrocher ou démissionner. Je ne vois pas dans le programme
de Sam des objectifs pour rendre Debian plus pertinente et plus viable dans
l'environnement d'aujourd'hui.

</p>

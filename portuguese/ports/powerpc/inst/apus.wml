#use wml::debian::template title="Porte para PowerPC (APUS)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="a9738774706d265243f4d1b6f43b411f8536f5c8"

<h1>Instalando o Debian GNU/Linux em uma máquina PowerPC APUS</h1>

<p>

O Apus é o "Amiga PowerUp System" e consiste de um computador Amiga,
A1200, A3000 ou A4000, junto com uma das placas aceleradoras PowerPC/m68k
da, agora fechada, companhia Phase5, as placas BlizzardPPC ou CyberStormPPC.

<h2>Problemas conhecidos</h2>
<p>

Existem alguns problemas envolvidos com bootstrap em sistemas que não usam
somente PowerUp ou WarpUp, mas algum híbrido mutante de ambos. Eu não
sou muito familiarizado(a) com esse negócio já que eu somente uso o powerup,
então eu espero que alguém contribua com uma descrição mais completa dele.
<p>

Também o controlador SCSI do CyberStormPPC ainda não é suportado pelo
Linux, então você não pode usar discos anexados a ele.

<h2>Particionando do lado do AmigaOS</h2>
<p>

<code>amiga-fdisk</code> é a variante fdisk para tabelas de partição RDB
usadas pelo hardware do Amiga. Ele está funcionando, mas eu recomendo que,
em vez dela, você use as ferramentas de particionamento do Amiga para fazer
isso pelo AmigaOS.
<p>

HDToolbox, sendo a ferramenta de particionamento oficial do Commodore, deve
ser instalada em cada sistema AmigaOS. Somente lançar o HDToolbox deve
particionar o disco IDE da interface onboard IDE. Se você quer
acessar o disco SCSI em seu controlador SCSI da placa BlizzardPPC, você
deve usar o comando "hdtoolbox blizzppcscsi.device".
<p>

Uma outra opção é usar SCSIConfig, o particionador do Phase5 que
está nos disquetes que vieram com sua placa aceleradora.
<p>

Você precisará definir o tipo de partição para personalizada e fornecer
os seguintes IDs de tipo de partição:
<pre>
  * Partição Linux: 0x4c4e5800
  * Partição Linux swap: 0x53575000
</pre>

<h2>Bootstrap</h2>
<p>

Você encontrará o programa <code>bootstrap</code> no diretório
<code>apus/bootstrap</code> da distribuição em disquetes de boot powerpc
(encontrados em /debian/dists/woody/main/disks-powerpc/current).
<p>

O programa <code>bootstrap</code> consiste de três programas. Todos
os três precisam ser executáveis e estar em seu path do AmigaOS. Eles
são o executável <code>bootstrap</code> e a parte ppcboot_wup ou ppcboot_pup,
que é na verdade o lançador (ppcboot_pup para o sistema powerup
e ppcboot_wup para o sistema warpup).
<p>

Você vai iniciar o <code>bootstrap</code> com uma linha como esta:
<pre>
\# bootstrap --apus "opções do kernel"
</pre>
onde "opções do kernel" são definidas nas próximas seções.
<p>

O <code>bootstrap</code> vai retornar algumas saídas, então vai apagar
a tela por 10 a 30 segundos e então você terá o console Linux.

<h3>O comando <code>bootstrap</code> do Debian</h3>
<p>

O atual comando <code>bootstrap</code> para lançar o instalador
do sistema Debian seria:
<pre>
\# bootstrap --apus -k apus/linux -r apus/images-1.44/root.bin root=/dev/ram
</pre>
Após a instalação, para lançar o Debian, use:
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3
</pre>
onde sda3 é minha partição root do Debian, mude-a para a partição
que está hospedando sua partição root.


<h2>Opções de kernel</h2>
<p>
Você precisará adicionar algumas opções de kernel a depender da sua
configuração atual, o que será explicado nas próximas seções.

<h3>Opções de placa gráfica</h3>
<p>

O dispositivo gráfico a ser usado é uma opção definida com video=. Alguns
exemplos são mostrados abaixo. Para habilitar os gráficos nativos no modo
vga (640x480):
<pre>
  video=amifb:vga
</pre>
Para habilitar a placa gráfica Bvision/CyberVision no modo 1152x864 a 60Hz,
com fontes SUN12x22:
<pre>
  video=pm2fb:mode:1152x864-60,font:SUN12x22
</pre>

Para desabilitar um dos dispositivos gráficos:
<pre>
  video=amifb:disable
</pre>
Você pode mapear consoles virtuais para diferentes dispositivos sendo usados.
Use
<tt>
  video=map:01
</tt>
para mapear o console virtual (vc) 1 para o dispositivo 0, vc 2 para o
dispositivo 1, e após isso repita o mesmo padrão (vc3 para o dispositivo 0,
vc4 para o dispositivo 1, etc.). Para mapear vc 1,2,3,5,6,7 para o dispositivo
0 e vc 4,8 para o dispositivo 1, você usaria
<pre>
  video=map:0001
</pre>


<h3>A opção nobats</h3>
<p>

Usuários(as) Blizzard com discos scsi precisarão usar a opção "nobats".
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 nobats
</pre>

<h3>A opção 60nsram</h3>
<p>
Pessoas com ram de 60ns também podem usar a opção 60nsram.
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 60nsram
</pre>

<h3>Opções de depuração</h3>
<p>
Se você estiver vivenciando problemas, você pode usar a opção debug para
especificar a saída de mensagens do console para ir para um console serial
ou para memória em vez do console normal. Isto é útil para depuração quando
a saída do kernel não vai para a saída de console.
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 60nsram debug=mem
</pre>
Então você pode ler o resultado com o utilitário bootmesg do diretório
apus/bootstrap.
<p>

Outra ferramenta útil é o utilitário dmesg, que retornará as informações
do depuração do processo de bootstrap.

<h2>Particularidades do Apus em <code>dbootstrap</code></h2>
<p>

Existem algumas diferenças específicas para apus no uso do
<code>dbootstrap</code>.

<h3>Particionando o disco rígido - <code>amiga-fdisk</code></h3>
<p>

A subarquitetura apus usa a ferramenta de particionamento
<code>amiga-fdisk</code>. Como observado acima, você também pode usar as
ferramentas de particionamento pelo lado do AmigaOS.

<h3>Instalando o kernel e módulos do SO</h3>
<p>

Na verdade, esta opção não está funcionando. Estou em um processo para
propor uma opção "instalar os módulos do SO" para substituí-la, mas
por enquanto você pode pular este passo. O kernel não é necessário de
qualquer forma, já que ele reside no 
[nota da tradução: o texto está interrompido neste ponto na versão original]
# <!-- FIXME Sven?? server? network? -->


<h3>Opções não aplicáveis ao apus</h3>
<p>

Bem, algumas opções simplesmente não fazem sentido no apus, então até que eu
as exclua do menu, somente ignore-as. Elas não devem funcionar de qualquer
forma.

<p>
Essas opções são:
<pre>
* Make System Bootable directly from the Hard Disk.
(Fazer o sistema inicializável diretamente do disco rígido)

* Make a Boot Floppy.
(Fazer um disquete de boot)

* Eject the Floppy.
(Ejetar o disquete)
</pre>

<h2>Links para informações adicionais</h2>
<p>

O documento oficial Linux-apus e o FAQ estão em:
<p>
<a href="http://sourceforge.net/projects/linux-apus/">
http://sourceforge.net/projects/linux-apus/</a>
<p>

Outra fonte de informações valiosas é o site web do Linux-m68k e o faq
encontrados em:

<p>
<a href="http://sourceforge.net/projects/linux-m68k/">
http://sourceforge.net/projects/linux-m68k/</a>
<p>

Lá você encontrará muitas informações relativas ao Linux na plataforma amiga
que são comuns com Linux-m68k e Linux-apus.

<h2>Conclusão</h2>
<p>

Bem, este pequeno guia tenta explicar todas as particularidades da instalação
Linux-apus do Debian. O resto dela é muito similar a qualquer outra
instalação Debian/powerpc, como também é similar à instalação genérica do
Debian. Então você encontrará informações adicionais no
<a href="$(DOC)/">diretório de documentação do Debian</a>, como também em outros
sites e documentações de informação genérica sobre Linux.

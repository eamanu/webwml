#use wml::debian::blend title="Download de imagens live"
#use wml::debian::blends::hamradio
#use "../navbar.inc"
#use wml::debian::translation-check translation="a2a93c6af14d9de0d3c3aa2b2d7fa4d06a48ee43"

<p>O Pure Blend Debian Hamradio produz <b>imagens live de DVD</b> que podem
ser usadas para testar o Pure Blend Hamradio em seu computador sem
ter que instalá-lo primeiro. As imagens também contêm um instalador que pode
ser usado para instalar o Debian junto com os pacotes do blend.</p>

<h2>Prévia do Blend Debian Hamradio Jessie</h2>

<p>Uma prévia do lançamento do DVD live do Blend Debian Hamradio está
disponível para download. Este é um lançamento não oficial, já que os
metapacotes do blend não estão incluídos no Debian Jessie.</p>

<p>A última versão estável é: <strong><stable-version/></strong>.</p>

<ul>
        <li><a href="<stable-amd64-url/>">imagem live de DVD amd64 (ISO)</a>
        <li><a href="<stable-i386-url/>">imagem live de DVD i386 (ISO)</a>
        <li><a href="<stable-source-url/>">imagem live de DVD do repositório-fonte (tar)</a>
</ul>

<p>Para imagens webboot, checksums e assinaturas GPG, veja a <a
href="https://cdimage.debian.org/cdimage/blends-live/">listagem completa
de arquivos</a>.</p>

<h2>Blend Debian Hamradio Stretch</h2>

<p>Em um futuro próximo, os DVDs live serão construídos para o stretch (a atual
versão teste (testing) do Debian), embora atualmente eles não estejam
disponíveis.</p>

<h2>Começando</h2>

<h3>Usando um DVD</h3>

<p>A maior parte dos sistemas operacionais modernos consegue queimar imagens
ISO em mídia DVD. O FAQ do CD Debian fornece instruções para queimar imagens
ISO usando o <a href="https://www.debian.org/CD/faq/index#record-unix">Linux</a>,
<a href="https://www.debian.org/CD/faq/index#record-windows">Windows</a> e <a
href="https://www.debian.org/CD/faq/index#record-mac">Mac OS</a>. Se você está
com dificuldades, use um motor de busca na web que ele fornecerá as respostas
que precisa.</p>

<h3>Usando um pendrive USB</h3>

<p>As imagens ISO são construídas como imagens híbridas, para que você possa
copiá-las diretamente para um pendrive USB sem precisar usar qualquer tipo
de software especial como unetbootin. Em um sistema Linux, você consegue
fazer isso desse modo:</p>

<pre>sudo dd if=/caminho/para/o/debian-hamradio-live-image.iso of=/dev/sd<b>X</b></pre>

<p>A saída do comando dmesg deve permitir que você descubra o nome do
dispositivo do pendrive USB, o qual você precisa para mudar o <b>X</b>
para a letra fornecida.</p>

#use wml::debian::template title="Informações de lançamento do Debian GNU/Linux 2.2 ('potato')" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/potato/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/info"
# $Id$
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9"

<p>Debian GNU/Linux 2.2 (a.k.a. Potato) foi lançado em
<:=spokendate ("2000-08-14"):>. A última versão do 2.2 é a
<current_release_potato>, lançada em <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strongO>O Debian GNU/Linux 2.2 tornou-se obsoleto quando o
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a> foi lançado.
As atualizações de segurança foram descontinuadas em 30 de junho de 2003.
</strong> Por favor, consulte
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
o anúncio do Time de Segurança</a> para maiores informações.</p>

<p>Para informações sobre grandes mudanças desse lançamento, consulte as
<a href="releasenotes">notas de lançamento</a> e o
<a href="$(HOME)/News/2000/20000815">comunicado à imprensa</a> oficial.</p>

<p>O Debian GNU/Linux 2.2 é dedicado à memória de Joel "Espy" Klecker,
um desenvolvedor Debian, conhecido pela maioria dos participantes do
projeto Debian. Ele estava de cama, lutando contra uma doença conhecida
como Distrofia Muscular Duchene durante a maior parte do tempo em que
se envolveu com o Debian. Somente agora o Projeto Debian percebeu
a extensão de sua dedicação e a amizade que ele conferiu a nós. Então,
como uma mostra de apreciação e em memória de sua vida inspiradora, esse
lançamento do Debian GNU/Linux é dedicado a ele.</p>

<p>O Debian GNU/Linux 2.2 está disponível através da Internet ou através de
vendedores de CD. Por favor, consulte a <a href="$(HOME)/distrib/">página da
distribuição</a> para maiores informações sobre como obter o Debian.</p>

<p>As seguintes arquiteturas são suportadas nesse lançamento:</p>

<ul>
<: foreach $arch (@arches) {
      print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
   } :>
</ul>

<p>Antes de instalar o Debian, por favor leia o <A HREF="installmanual">\
manual de instalação</A>. O manual de instalação para a arquitetura desejada
contém instruções e links para todos os arquivos que você necessita para a
instalação. Você pode também se interessar pelo <a href="installguide/">\
guia de instalação do Debian 2.2</a>, que é um tutorial online.</p>

<p>Se você está usando o APT, você pode usar as linhas a seguir no seu
arquivo <code>/etc/apt/sources.list</code> para poder acessar os pacotes
do potato:</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Leia as páginas de manual do <code>apt-get</code>(8) e do
<code>sources.list</code>(5) para mais informações.</p>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada estável (stable). Nós fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>A integridade dos dados é garantida pelo arquivo <code>Release</code>
digitalmente assinado. Para assegurar que todos os arquivos que estão no
lançamento pertençam realmente a ele, os checksums MD5 de todos os arquivos
<code>Packages</code> são copiados para dentro do arquivo <code>Release</code>.
Assinaturas digitais para esse arquivo são armazenadas no arquivo
<code>Release.gpg</code>, usando esta chave: <a href="https://ftp-master.debian.org/ziyi_key_2002.asc">\
ftpmaster</a>.</p>

<p>Para verificar o arquivo <code>Release</code>, você terá que pegar
ambos os arquivos e executar <code>gpg --verify Release.gpg Release</code>
depois que importar as chaves que são usadas para assinar estes arquivos.</p>

<p>Por último mas não menos importante, nós temos uma lista de <a href="credits">pessoas
que recebem crédito</a> por fazerem este lançamento acontecer.</p>

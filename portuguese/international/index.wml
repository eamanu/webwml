#use wml::debian::template title="Debian Internacional"
#use wml::debian::translation-check translation="c983d05b92b263f99491afdfdb7b5eb303d0cd2c" maintainer="Tassia Camoes Araujo"

<p>Nas comunidades de software livre pela Internet, nós nos comunicamos
principalmente através do idioma inglês. A maior parte de nosso trabalho é
feita em inglês, mas espera-se que, um dia, toda a documentação, o software e a
instalação do Debian estejam disponíveis em todos os idiomas. Estamos
constantemente trabalhando para atingirmos esse objetivo. No entanto, há ainda
muitos obstáculos.

<H2>O site do Debian e a negociação de conteúdo</H2>

<P>O site web do Debian usa a <A href="../intro/cn">negociação de conteúdo</A>
para automaticamente entregar as páginas web no seu idioma de preferência -
se o seu navegador estiver configurado apropriadamente e se a página existir no
seu idioma. Nós criamos uma página que explica
<A href="../intro/cn#howtoset">como configurar o idioma preferencial no seu
navegador</A>.

<H2>Páginas do Debian específicas para cada idioma</H2>

<P>Essas páginas contêm informações sobre como usar o Debian em idiomas que não
a língua inglesa. Cada página contém informações sobre os recursos do Debian
que estão disponíveis naquele idioma específico, incluindo as listas de e-mail.

#include "$(ENGLISHDIR)/international/index.data"

<H2>Adicionando suporte a um novo idioma</H2>

<P>O Debian é mantido completamente por voluntários(as). Se nós não suportamos
o seu idioma atualmente, você deve considerar fazer um esforço para isso.
Para prover de forma completa um idioma, as seguintes áreas do Debian devem
ser trabalhadas:
<dl>
  <dt><strong>Sistema de instalação</strong></dt>
      <dd>Por favor, veja as
      <a href="https://salsa.debian.org/installer-team/installation-guide/blob/master/doc/translations.txt">\
      informações para tradutores(as)</a> no
      <a href="https://salsa.debian.org/installer-team/installation-guide">repositório
      do guia de instalação</a>.
      O trabalho relativo ao sistema de instalação do Debian é discutido na
      lista de e-mail <a href="https://lists.debian.org/debian-boot/">\
      <tt>debian-boot</tt></a>.
      </dd>
  <dt><strong>Documentação do Debian</strong></dt>
      <dd>Por favor, veja as <a href="$(HOME)/doc/ddp">páginas do projeto de
      documentação</a>. Este tópico é discutido na lista de e-mails
      <a href="https://lists.debian.org/debian-doc/"><tt>debian-doc</tt></a>.
      </dd>
  <dt><strong>Páginas web do Debian</strong></dt>
      <dd>Por favor, veja as <a href="$(DEVEL)/website/translating">páginas
      para tradutores(as) do site web do Debian</a>. Este tópico é discutido na
      lista de e-mails
      <a href="https://lists.debian.org/debian-www/"><tt>debian-www</tt></a>.
      </dd>
  <dt><strong>Páginas wiki do Debian</strong></dt>
      <dd>Por favor, veja a <a href="$(DEVEL)/website/translating">seção de
      tradução no guia de edição da wiki do Debian</a>. Este tópico é discutido
      na lista de e-mails
      <a href="https://lists.debian.org/debian-www/"><tt>debian-www</tt></a>.
      </dd>
  <dt><strong>Idiomas especiais</strong></dt>
      <dd>Alguns idiomas requerem que softwares tenham características
      especiais em vários aspectos como exibição, entrada, impressão,
      tratamento de string, processamento de texto, etc.
      Idiomas do leste asiático (chinês, japonês e coreano) precisam de suporte
      a multibyte e métodos de entrada especiais. Os idiomas tailandês,
      indiano e outros precisam de suporte a caracteres combinados. Idiomas RTL
      ("right-to-left" ou direita-para-esquerda) como árabe e hebreu precisam de
      suporte bidi (bidirecional). Note que esses requerimentos são precondições
      para todas as outras partes da i18n e da l10n desses idiomas, apesar de
      ser uma parte muito difícil da i18n. Este tópico é discutido na lista de
      e-mails
      <a href="https://lists.debian.org/debian-i18n/"><tt>debian-i18n</tt></a>.
      </dd>
</dl>
# De forma complementar, alguns idiomas talvez precisem de pacotes de software
# para suportar seu conjunto de caracteres. Isto é mais provável para idiomas
# com conjuntos de caracteres multibyte.

<P>Suportar um idioma é um esforço contínuo. Simplesmente traduzir alguns
documentos não é o bastante. Tudo muda com o tempo e toda documentação
deve ser mantida atualizada. Por isso, sugere-se que as pessoas se reúnam em
grupos para dividir o trabalho. Uma boa maneira de começar é
<a href="$(HOME)/MailingLists/HOWTO_start_list">solicitar uma lista de discussão
de usuários(as) do Debian</a> para o seu idioma.

<H2>Central de estatísticas de traduções do Debian</H2>

<P>Nós mantemos <a href="l10n/">estatísticas de traduções</a> que
contêm dados sobre os conjuntos de mensagens traduzidas nos pacotes
Debian.

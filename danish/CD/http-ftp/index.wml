#use wml::debian::cdimage title="Hent Debians cd-/dvd-aftryk via HTTP/FTP" BARETITLE=true
#use wml::debian::translation-check translation="f4fe84f1063f34ac4bfb75eb963e6fa71ba0e642"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>Hent ikke cd- eller dvd-aftryk med din webbrowser, på den måde du 
henter andre filer!</strong> Årsagen er at hvis din overførsel stopper, så kan 
de fleste browsere ikke genoptage overførslen hvor den stoppede.</p>
</div>

<p>Brug i stedet et værktøj, som giver mulighed for at genoptage en overførsel - 
typisk beskrevet som en <q>download manager</q>.  Der er mange browserplugins, 
som gør dette, ellers kan du installere et separat program.  Under Linux/Unix 
kan du bruge <a href="http://aria2.sourceforge.net/">aria2</a>, 
<a href="http://dfast.sourceforge.net/">wxDownload Fast</a> eller (på 
kommandolinjen) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> eller
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.  Der er 
opremset mange flere muligheder i en 
<a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">\
sammenligning af download managers</a>.</p>

<p>Følgende Debian-aftryk er tilgængelige:</p>

<ul>
    <li><a href="#stable">Officielle cd-/dvd-aftryk af udgaven 
	<q>stable</q></a></li>
    <li><a href="#firmware"><b>Uofficielle</b> cd-/dvd-aftryk af udgaven 
	<q>stable</q>, med medfølgende <b>ikke-fri</b> firmware</a></li>
    <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Officielle 
	cd-/dvd-aftryk af distributionen <q>testing</q> (<em>genfremstilles 
	ugentligt</em>)</a></li>
<comment>
    <li>Uofficielle cd-dvd-aftryk med distributionerne <q>testing</q> og <q>unstable</q>
	fremstillet af fsn://HU &ndash; <a href="#unofficial">se nedenfor</a></li>
</comment>

</ul>

<p>Se også:</p>

<ul>
  <li>En komplet <a href="#mirrors">liste over <tt>debian-cd/</tt>-spejle</a></li>

  <li>Vedrørende aftryk til <q>netværksinstallering</q> (150-300&nbsp;MB), se 
      siden om <a href="../netinst/">netværksinstallering</a>.</li>

  <li>Vedrørende <q>netinst</q>-aftryk af distributionen 
      <q>testing</q> er der både daglige opbygninger og kendte, fungerende 
      øjebliksbilleder, se siden om <a href="$(DEVEL)/debian-installer/">\
      Debian-Installer</a>.</li>
</ul>

<hr />

<h2><a name="stable">Officielle cd-/dvd-aftryk af udgaven <q>stable</q></a></h2>

<p>For at installere Debian på en maskine uden en internetforbindelse, kan man
anvende cd-aftryk (hver på 700 megabyte) eller dvd-aftryk (hver på 4,7 
gigabyte).  Hent den første cd- eller dvd-aftryksfil, brænd den vha. en 
cd-/dvd-brænder (eller skriv den til en USB-pind vedrørende tilpasningerne i386 
og amd64) og genstart dernæst computeren derfra.</p>

<p>Den <strong>første</strong> cd-/dvd-skive indeholder alle filer, som er 
nødvendige for at installere et standard-Debian-system.<br />
For at undgå at hente unødvendige data, hent <strong>ikke</strong> andre cd- 
eller dvd-aftryk, med mindre du ved, at du skal bruge pakker på dem.</p>

<div class="line">
<div class="item col50">
<p><strong>Cd</strong></p>

<p>De følgende links henviser til aftryksfiler, som er op til 700 megabyte i 
størrelse, hvilket gør at de kan brændes til almindelige cd-r(w)-medier:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>Dvd</strong></p>

<p>De følgende links henviser til aftryksfiler, som er op til 4,7 gigabyte i
størrelse, hvilket gør at de kan brændes til almindelige dvd-r-/dvd+r- og 
lignende medier:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Sørg for at have kigget i dokumentationen før du installerer.
<strong>Læser du kun ét dokuement</strong> før installeringen, se læs vores
<a href="$(HOME)/releases/stable/i386/apa">Installation Howto</a>, en hurtig
gennemgang af installeringsprocessen.  Andre nyttig dokumentation er:</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    den detaljerede installeringsvejledning</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    Documentation</a>, indeholder OSS'en med ofte stillede og svar</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, listen over kendte problemer med installeringsprogrammet</li>
</ul>

<hr />

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<h2><a name="firmware">Uofficielle cd-/dvd-aftryk med medfølgende ikke-fri firmware</a></h2>

<div id="firmware_nonfree" class="important">

<p>Hvis noget af hardwaren i dit system <strong>kræver at ikke-fri firmware skal 
indlæses</strong> sammen med enhedsdriveren, kan du anvende en 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarball med almindelige firmwarepakker</a> eller hente et 
<strong>uofficielt</strong> aftryk, som indehodler denne 
<strong>ikke-frie</strong> firmware.  Vejledning i hvordan en tarball anvendes 
og generelle oplysninger om indlæsning af firmware under en installering, finder 
man i <a href="../../releases/stable/amd64/ch06s04">Installation Guide</a>.</p>

<p><a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">\
uofficielle installeringsaftryk til udgaven <q>stable</q> med medfølgende firmware</a></p>

</div>

<hr />

<h2><a name="mirrors">Registrerede filspejlinger af arkivet <q>debian-cd</q></a></h2>

<p>Bemærk at <strong>nogle filspejle ikke er ført ajour</strong> &ndash; før du
henter noget, så kontrollér at versionsnummeret på cd-aftrykkene er de samme
som dem der er anført <a href="../#latest">her på webstedet</a>!
Bemærk desuden at mange filspejle ikke fører det komplette udvalg af aftryk
(særlig dvd-aftrykkene), på grund af størrelsen.</p>

<p><strong>Hvis du er i tvivl, så brug den <a href="https://cdimage.debian.org/debian-cd/">\
primære server i Sverige med cd-aftryk</a></strong>, eller prøv den 
<a href="http://debian-cd.debian.net/">eksperimentelle automatiske 
filspejlsvælger</a>, der automatisk viderestiller dig til et filspejl i 
nærheden, som vides at have den aktuelle version.</p>

<p>Er du interesseret i at tilbyde Debians cd-aftryk på dit filspejl? Hvis ja,
så se <a href="../mirroring/">instruktionerne i at opsætte et filspejl af
cd-aftrykkene</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"

#use wml::debian::template title="پشتیبانی"
#use wml::debian::toc
#use wml::debian::translation-check translation="76ce7e7f5cfd0e2f3e8931269bdcd1f9518ee67f" maintainer="Seyed mohammad ali Hosseinifard"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

دبیان و پشتیبانی آن توسط جامعه‌ای از داوطلبان اداره می شود.

اگر این پشتیبانی جامعه محور نیازهای شما را برآورده نمی‌کند،
می‌توانید <a href="doc/">اسناد</a> or ما را بخوانید
یا یک <a href="consultants/">مشاور</a> استخدام کنید.

<toc-display />

<toc-add-entry name="irc">راهنمایی برخط بی‌درنگ با استفاده از آی‌آرسی</toc-add-entry>

<p><a href="http://www.irchelp.org/">آی‌آرسی (Internet Relay Chat)</a> راهی برای
گفتگوی اینترنتی با مردم سراسر جهان بصورت بی‌درنگ است.
کانال‌های آی‌آرسی اختصاص داده شده به دبیان را می توان در
<a href="https://www.oftc.net/">OFTC</a> یافت.</p>

<p>برای اتصال، شما نیاز به یک کارخواه آی‌آرسی دارید. برخی از محبوب‌ترین کارخواه‌ها
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>،
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>،
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>،
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> و
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a> هستند
که همه آنها برای دبیان بسته‌بندی شده‌اند.
OFTC همچنین یک <a href="https://www.oftc.net/WebChat/">رابط وب</a>
ارائه می‌دهد که به شما امکان می‌دهد بدون نیاز به نصب هیچ کارخواه محلی‌ای،
با یک مرورگر به آی‌آرسی متصل شوید.</p>

<p>هنگامی که کارخواه را نصب کردید، باید به آن بگویید که به
کارساز متصل شود. در اکثر کارخواه‌ها، می‌توانید این کار را با تایپ کردن فرمان زیر انجام دهید:</p>

<pre>
/server irc.debian.org
</pre>

<p>در برخی کارخواه‌ها (مانند irssi) باید به جای آن فرمان زیر را تایپ کنید:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
<p>پس از آنکه متصل شدید، برای دریافت پشتیبانی فارسی با فرمان</p>
<pre>/join #debian-ir</pre>
# for support in your language.
<p>به کانال <code>#debian-ir</code> بپیوندید.</p>

<p>برای پشتیبانی به زبان انگلیسی نیز با تایپ فرمان زیر وارد کانال <code>#debian</code> شوید</p>

<pre>
/join #debian
</pre>

<p>نکته: کارخواه‌هایی مانند HexChat
معمولاً دارای یک رابط گرافیکی برای پیوستن به کارسازها و کانال‌ها هستند.</p>

<p>پس از انجام مراحل بالا، شما در میان جمع دوستانه
<code>#debian</code> یا <code>#debian-ir</code> خواهید بود و می‌توانید سوالات خود پیرامون دبیان
را در آنجا بپرسید. سوالات متداول درمورد کانال‌ها در
<url "https://wiki.debian.org/DebianIRC" /> موجود است.</p>


<p>شبکه‌های آی‌آرسی دیگری نیز وجود دارند که در آنها هم می‌وانید درمورد دبیان گفتگو کنید.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">فهرست‌های پستی</toc-add-entry>

<p>دبیان توسط افرادی در سراسر جهان توسعه می‌یابد. از این رو، ایمیل یک روش
ترجیهی برای گفتگو درمورد موضوعات گوناگون می‌باشد.
بسیاری از مکالمات بین توسعه‌دهندگان و کاربران دبیان
ازطریق چند فهرست پستی انجام می‌شود.</p>

<p>چندین فهرست پستی همگانی وجود دارد. برای اطلاعات بیشتر
صفحه <a href="MailingLists/">فهرست‌های پستی دبیان</a>را ببینید.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
برای پشتیبانی به زبان انگلیسی، لطفا به
<a href="https://lists.debian.org/debian-user/">فهرست پستی debian-user</a> نامه بزنید.
</p>

<p>
برای پشتیبانه به سایر زبان‌ها،
<a href="https://lists.debian.org/users.html">mفهرست‌های پستی برای کاربرن</a> را بررسی کنید.
</p>

<p>البته تعداد زیادی فهرست پستی اختصاص یافته به جنبه‌های مختلف
اکوسیستم لینوکس وجود دارند که ویژه دبیان نیستند.
از موتور جستجوی دلخواه‌تان برای یافتن فهرست مناسب نیازهای خود استفاده کنید.</p>


<toc-add-entry name="usenet">گروه‌های خبری یوزنت</toc-add-entry>

<p>بسیاری از <a href="#mail_lists">فهرست‌های پستی</a> ما را می‌توان به عنوان
گروه خبری در سلسله مراتب  <kbd>linux.debian.*</kbd> مرور کرد.
این کار را می‌توان با استفاده از یک رابط وب مانند
<a href="https://groups.google.com/forum/">گروه‌های گوگل</a> نیز نجام داد.


<toc-add-entry name="web">وبسایت‌ها</toc-add-entry>

<h3 id="forums">انجمن‌ها</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>


<p><a href="http://forums.debian.net">انجمن‌های کاربری دبیان</a> یک درگاه اینترنتی است
که در آن می‌توانید درمورد موضوعات مرتبط با دبیان گفتگو کنید،
درمورد دبیان سوال بپرسید و از سایر کاربران پاسخ بگیرید.</p>


<toc-add-entry name="maintainers">تماس با نگهدارندگان بسته</toc-add-entry>

<p>دو راه برای تماس با نگهدارنده‌های بسته وجود دارد. اگر می‌خواهید اشکال
موجود در بسته‌ای را به نگهدارنده‌اش گزارش دهید، تنها یک گزارش اشکال ارسال کنید (بخش سامانه ردیابی
اشکال را در پایین ببینید). نگهدارنده یک نسخه از گزارش شما را دریافت خواهد کرد.</p>

<p>اگر فقط می‌خواهید با نگهدارنده ارتباط برقرار کنید، می‌توانید از ایمیل‌های مستعار موجود
برای هر بسته استفاده نمایید. تمام ایمیل‌های ارسال شده به
&lt;<em>نام بسته</em>&gt;@packages.debian.org به مسئول نگهداری آن بسته
ارجاع داده می‌شوند.</p>


<toc-add-entry name="bts" href="Bugs/">سامانه ردیابی اشکال</toc-add-entry>

<p>توزیع دبیان دارای یک سامانه ردیابی اشکال است که در آن جزئیات اشکالات گزارش شده
توسط کاربران و توسعه‌دهندگان ثبت می‌شود. به هر اشکال شماره‌ای اختصاص داده شده
و تا زمانی‌که به آن رسیدگی نشود در سامانه فعال خواهد ماند.</p>

<p>برای گزارش اشکال، می‌توانید از صفحه‌ای که در پایین ذکر شده دیدن کنید. ما پیشنهاد می‌کنیم
از بسته <q>reportbug</q> برای گزارش خودکار اشکال استفاده کنید.</p>

<p>اطلاعات مربوط به نحوه ارسال اشکالات، مشاهده اشکالات فعال فعلی و
به طول کلی سامانه ردیابی اشکال را می‌توان در
<a href="Bugs/">صفحات وب سامانه ردیابی اشکال</a> یافت.</p>


<toc-add-entry name="consultants" href="consultants/">مشاوران</toc-add-entry>

<p>دبیان یک نرم‌افزار رایگان است و از طریق فهرست‌های پستی پشتیبانی رایگان ارائه می‌دهد. اما
با این حال، برخی از افراد به دلیل نداشتن زمان کافی یا نیازهای تخصصی،
مایلند شخصی را برای نگهداری یا افزودن قابلیت‌های اضافی به سیستم دبیان خود استخدام کنند.
در <a href="consultants/">صفحه مشاوران</a>، فهرستی از افراد و شرکت‌هایی را که چنین خدماتی
ارائه می‌دهند گردآوری نموده‌ایم.</p>


<toc-add-entry name="release" href="releases/stable/">مشکلات شناخته شده</toc-add-entry>

<p>محدودیت‌ها و مشکلات جدی توزیع پایدار فعلی (درصورت وجود)
در <a href="releases/stable/">صفحات انتشار</a>. شرح داده شده اند.</p>

<p>به <a href="releases/stable/releasenotes">یادداشت‌های انتشار</a>
و <a href="releases/stable/errata">Errata</a> (فهرست خطاها) توجه ویژه‌ای داشته باشید.</p>

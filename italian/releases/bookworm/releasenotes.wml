#use wml::debian::template title="Debian 12 &mdash; Note di rilascio" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="33765739bb1fc08542881115f973fb8bd4ab5777" maintainer="Luca Monducci"

<if-stable-release release="buster">
<p>Le Note di rilascio per Debian 12, nome in codice <q>bookworm</q>, non
esistono ancora.</p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>Questa è una <strong>versione di sviluppo</strong> delle Note di rilascio
per Debian 12, nome in codice <q>bookworm</q>, che non è stata ancora
rilasciata. Le informazioni contenute potrebbero essere non corrette e non
aggiornate inoltre è prevalentemente incompleta.</p>
</if-stable-release>

<p>Per scoprire cosa c'è nuovo in Debian 12, vedere le note di rilascio per
la propria architettura:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Note di rilascio'); :>
</ul>

<p>Le note di rilascio contengono le istruzioni per gli utenti che intendono
aggiornare i propri sistemi.</p>

<p>Se il browser è stato configurato con la lingua corretta, utilizzando
i precedenti collegamenti si dovrebbe arrivare automaticamente alla versione
HTML del documento tradotta nella propria lingua; si veda <a
href="$(HOME)/intro/cn">negoziazione dei contenuti</a>. Altrimenti scegliere
quello relativo all'architettura, alla lingua e al formato che interessa
dalla tabella sottostante.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architettura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Lingua</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>

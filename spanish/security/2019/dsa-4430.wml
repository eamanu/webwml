#use wml::debian::translation-check translation="907a0371eb05342911768c66ad56f028349d3301"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Mathy Vanhoef (NYUAD) y Eyal Ronen (Universidad de Tel Aviv y KU Leuven) encontraron
múltiples vulnerabilidades en la implementación de WPA en wpa_supplication
(estación) y en hostapd (punto de acceso). Estas vulnerabilidades son también conocidas
colectivamente como <q>Dragonblood</q> («Sangre de dragón»).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

    <p>Ataque de canal lateral a la caché contra la implementación de EAP-pwd: un
    atacante con capacidad para ejecutar código sin privilegios en la máquina objetivo (incluyendo, por
    ejemplo, código javascript en un navegador o en un teléfono inteligente) durante el «handshake»
    podría deducir información suficiente para descubrir la contraseña en un ataque de
    diccionario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

    <p>Ataque de reflejo («reflection attack») contra la implementación del servidor EAP-pwd: una falta de
    validación de los valores de escalares o elementos recibidos en los mensajes EAP-pwd-Commit
    podría dar lugar a ataques por los que se podría completar el intercambio
    de autenticación EAP-pwd sin que el atacante necesitara conocer la contraseña.
    Esto no tiene como consecuencia que el atacante pueda derivar la clave de sesión,
    completar el intercambio de claves subsiguiente y acceder a la red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

    <p>Ausencia de validación de los commits de escalares y elementos en el servidor EAP-pwd: hostapd
    no valida los valores recibidos en el mensaje EAP-pwd-Commit, por lo que un
    atacante podría utilizar un mensaje de commit modificado para manipular el
    intercambio de manera que hostapd derive la clave de sesión a partir de un conjunto limitado de
    valores posibles. Esto podría dar lugar a que el atacante complete
    la autenticación y acceda a la red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

    <p>Ausencia de validación de los commits de escalares y elementos en el par EAP-pwd: wpa_supplicant
    no valida los valores recibidos en el mensaje EAP-pwd-Commit, por lo que un
    atacante podría utilizar un mensaje de commit modificado para manipular el
    intercambio de manera que wpa_supplicant derive la clave de sesión a partir de un conjunto
    limitado de valores posibles. Esto podría dar lugar a que el atacante
    complete la autenticación y actúe como un AP deshonesto.</p></li>

</ul>

<p>Tenga en cuenta que el apodo Dragonblood también se aplica a
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9494">\
CVE-2019-9494</a> y a <a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">\
CVE-2014-9496</a>, que son vulnerabilidades en el protocolo SAE de WPA3. SAE no está
habilitado en las compilaciones de wpa para Debian stretch, que, por lo tanto, no es vulnerable por omisión.</p>

<p>Debido a la complejidad del proceso de adaptación, la corrección para estas
vulnerabilidades es parcial. Se aconseja a los usuarios el uso de contraseñas fuertes para
prevenir ataques de diccionario o utilizar una versión basada en la 2.7 obtenida en stretch-backports
(versión superior a la 2:2.7+git20190128+0c1e29f-4).</p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 2:2.4-1+deb9u3.</p>

<p>Le recomendamos que actualice los paquetes de wpa.</p>

<p>Para información detallada sobre el estado de seguridad de wpa, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4430.data"
